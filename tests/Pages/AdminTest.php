<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Users;

class AdminTest extends TestCase
{

    protected $_user;

    public function setUp()
    {
        parent::setUp();
//        $this->actingAs(Users::where('group_id', Users::GROUP_ADMIN)->first());
    }

    /**
     * Test admin page
     *
     * @return void
     */
    public function testAdminPage()
    {
        return $this->assertTrue(true);
        $this->get(route('admin.dashboard'))
            ->assertStatus(302);
    }
}
