<?php

namespace Tests\Feature;

use Tests\TestCase;

class GuestTest extends TestCase
{
    /**
     * Test redirect to login page
     *
     * @return void
     */
    public function testMainPage()
    {
        $response = $this->get('/');
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * Test login page
     *
     * @return void
     */
    public function testLoginPage()
    {
        $response = $this->get(route('login'));
        $response->assertStatus(200);
    }

    /**
     * Test admin page
     *
     * @return void
     */
    public function testAdminPage()
    {
        $response = $this->get(route('admin.dashboard'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * Test forgot password page
     *
     * @return void
     */
    public function testForgotPage()
    {
        $response = $this->get(url('password/reset'));
        $response->assertStatus(200);
    }
}
