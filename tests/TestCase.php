<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Applications;
use App\Models\Statistics;
use Carbon\Carbon;

abstract class TestCase extends BaseTestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    use CreatesApplication;

    /**
     * Application
     * @var \App\Models\Applications
     */
    protected $_application;

    /**
     * Language
     * @var \App\Models\Languages
     */
    protected $_language;

    /**
     * Screen size
     * @var \App\Models\Screensizes
     */
    protected $_screensize;

    /**
     * Company
     * @var \App\Models\Companies
     */
    protected $_company;

    /**
     * Creative
     * @var \App\Models\Creatives
     */
    protected $_creative;

    /**
     * Banner
     * @var \App\Models\Banners
     */
    protected $_banner;

    /**
     * Statistic
     * @var \App\Models\Statistics
     */
    protected $_statistic;

    /**
     * Faker
     * @var \Faker\Factory
     */
    protected $_faker;

    /**
     * Set up test case
     */
    public function setUp()
    {
        parent::setUp();
        $this->_faker = \Faker\Factory::create();
    }

    /**
     * Get application data provider
     * @return \App\Models\Applications
     */
    protected function applicationDataProvider()
    {
        $this->_application = factory(Applications::class)->create([
            'bundle_id_ios' => $this->_faker->word(),
            'bundle_id_android' => $this->_faker->word()
        ]);
    }

    /**
     * Get language data provider
     * @return \App\Models\Languages
     */
    protected function languageDataProvider()
    {
        $this->_language = factory(\App\Models\Languages::class)->create([
            'title' => '11'
        ]);
    }

    /**
     * Get screen size data provider
     * @return \App\Models\Screensizes
     */
    protected function screensizeDataProvider()
    {
        $this->_screensize = factory(\App\Models\Screensizes::class)->create();
    }

    /**
     * Get company data provider
     * @return \App\Models\Companies
     */
    protected function companyDataProvider()
    {
        if (!$this->_application)
            $this->applicationDataProvider();

        $this->_company = factory(\App\Models\Companies::class)->create([
            'active' => true,
            'end_date' => Carbon::now()->addYear(1)
        ]);

        $this->_company->applications()->attach($this->_application);
    }

    /**
     * Get creative data provider
     * @return \App\Models\Creatives
     */
    protected function creativeDataProvider()
    {
        if (!$this->_company)
            $this->companyDataProvider();

        $this->_creative = factory(\App\Models\Creatives::class)->create();
        $this->_creative->companies()->attach($this->_company);
    }

    /**
     * Get banner data provider
     * @return \App\Models\Banners
     */
    protected function bannerDataProvider()
    {
        $this->_banner = factory(\App\Models\Banners::class)->create([
            'creative_id' => $this->_creative->id,
            'language_id' => $this->_language->id,
            'screensize_id' => $this->_screensize->id,
        ]);
    }

    /**
     * Get statistic by current params
     * @return \App\Models\Statistics
     */
    protected function getStatistic()
    {
        if (!$this->_statistic) {
            $statistic = Statistics::where('application_id', $this->_application->id)
                ->where('company_id', $this->_company->id)
                ->where('creative_id', $this->_creative->id)
                ->where('platform', $this->_application->platform)
                ->where('date', Carbon::now()->toDateString())
                ->first();
            if (!$statistic) {
                $statistic = new Statistics;
                $statistic->company_id = $this->_company->id;
                $statistic->creative_id = $this->_creative->id;
                $statistic->platform = $this->_application->platform;
                $statistic->date = Carbon::now()->toDateString();
                $statistic->conversion = 1;
                $statistic->click = 1;
                $statistic->view = 1;
                $statistic->viewed = 1;
                $statistic->save();
            };
            $this->_statistic = $statistic;
        }

        return $this->_statistic;
    }
}
