<?php

namespace Tests\Unit\Api\V1;

use Tests\TestCase;

use Illuminate\Http\Response;

class FrequenceTest extends TestCase
{
    /**
     * @var string
     */
    const URL = '/api/v1/frequence';

    /**
     * Set up test case
     */
    public function setUp()
    {
        parent::setUp();
        $this->applicationDataProvider();
    }

    /**
     * Testing empty response
     * @return void
     */
    public function testEmptyResponse()
    {
        $response = $this->call('GET', self::URL);
        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(['status', 'errors' => ['platform', 'bundle_id']]);
    }

    /**
     * Testing validation
     * @return void
     */
    public function testValidationResponse()
    {
        $response = $this->call('GET', self::URL, [
            'bundle_id' => $this->_faker->text(1024),
            'platform' => $this->_faker->word()
        ]);
        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(['status', 'errors' => ['platform', 'bundle_id']]);
    }

    /**
     * Testing not found application by bundle_id
     * @return void
     */
    public function testNotFoundApplicationResponse()
    {
        $response = $this->call('GET', self::URL, [
            'bundle_id' => md5(time()),
            'platform' => $this->_faker->randomElement(['android', 'ios'])
        ]);
        $response->assertStatus(Response::HTTP_NOT_FOUND)
            ->assertJsonStructure(['status', 'errors' => ['application']]);
    }

    /**
     * Testing success response
     * @return void
     */
    public function testSuccessResponse()
    {
        $platform = $this->_faker->randomElement(['android']);

        $data = [
            'bundle_id' => $this->_application->bundle_id_android,
            'platform' => $platform
        ];

        $response = $this->call('GET', self::URL, $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertExactJson([
                "status" => true,
                "frequence_banner_time" => strval($this->_application->frequence_banner_time),
                "frequence_banner_session" => strval($this->_application->frequence_banner_session),
                "frequence_video_time" => strval($this->_application->frequence_video_time),
                "frequence_video_session" => strval($this->_application->frequence_video_session),
                "frequence_other_time" => strval($this->_application->frequence_other_time),
                "frequence_other_session" => strval($this->_application->frequence_other_session)
            ])
            ->assertHeader('Last-Modified');
    }
}