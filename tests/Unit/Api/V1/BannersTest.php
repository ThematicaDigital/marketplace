<?php

namespace Tests\Unit\Api\V1;

use App\Models\Banners;
use Tests\TestCase;
use Illuminate\Http\Response;

use App\Models\Companies;
use App\Models\Applications;

class BannersTest extends TestCase
{
    /**
     * @var string
     */
    const URL = '/api/v1/banners';

    /**
     * Set up test case
     */
    public function setUp()
    {
        parent::setUp();
        $this->languageDataProvider();
        $this->screensizeDataProvider();
        $this->applicationDataProvider();
        $this->companyDataProvider();
        $this->creativeDataProvider();
        $this->bannerDataProvider();
    }

    /**
     * Testing empty response
     * @return void
     */
    public function testEmptyResponse()
    {
        $response = $this->call('GET', self::URL);
        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(['status', 'errors', 'errors' => [
                'bundle_id',
                'platform',
                'resolution',
                'connection_type'
            ]]);
    }

    /**
     * Testing validation response
     * @return void
     */
    public function testValidationResponse()
    {
        $data = [
            'bundle_id' => $this->_faker->text(1024),
            'platform' => $this->_faker->word(),
            'locale' => $this->_faker->word(),
            'resolution' => $this->_faker->word(),
            'connection_type' => $this->_faker->word(),
        ];
        $response = $this->call('GET', self::URL, $data);
        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(['status', 'errors', 'errors' => [
                'bundle_id',
                'platform',
                'locale',
                'resolution',
                'connection_type'
            ]]);
    }

    /**
     * Testing not found application by bundle_id
     * @return void
     */
    public function testNotFoundApplicationResponse()
    {
        $platform = $this->_faker->randomElement(['android', 'ios']);
        $data = [
            'bundle_id' => md5(time()),
            'platform' => $platform,
            'locale' => $this->_language->title,
            'resolution' => $this->_screensize->width . 'x' . $this->_screensize->height,
            'connection_type' => $this->_faker->randomElement(['wifi', 'mobile']),
        ];
        $response = $this->call('GET', self::URL, $data);

        $response->assertStatus(Response::HTTP_NOT_FOUND)
            ->assertJsonStructure(['status', 'errors']);
    }

    /**
     * Testing success response
     * @return void
     */
    public function testSuccessResponse()
    {
        $resolution = $this->_banner->screensize->width . 'x' . $this->_banner->screensize->height;
        $platform = Applications::decodePlatform($this->_company->platform);

        $data = [
            'bundle_id' => $platform == 'ios' ? $this->_application->bundle_id_ios : $this->_application->bundle_id_android,
            'platform' => $platform,
            'locale' => $this->_banner->language->title,
            'resolution' => $resolution,
            'connection_type' => Companies::decodeConnectionType($this->_company->connection_type),
        ];

        $response = $this->call('GET', self::URL, $data);
        $response->assertStatus(Response::HTTP_OK)
            ->assertExactJson([
                'status' => true,
                'count' => 1,
                'banners' => [
                    $this->_banner->creative->id => [
                        'id' => $this->_banner->id,
                        'type' => $this->_banner->type == Banners::TYPE_BANNER ? 'banner' : 'video',
                        'image' => !empty($this->_banner->image) ? cdn($this->_banner->image) : null,
                        'video' => !empty($this->_banner->video) ? cdn($this->_banner->video) : null,
                        'link' => $this->_banner->link,
                        'campaign_id' => strval($this->_company->id)
                    ]
                ],
                'campaigns' => [
                    [
                        'id' => strval($this->_company->id),
                        'priority' => strval($this->_company->priority)
                    ]
                ]
            ]);
    }
}