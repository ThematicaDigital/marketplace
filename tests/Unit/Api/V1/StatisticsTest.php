<?php

namespace Tests\Unit\Api\V1;

use Tests\TestCase;
use Illuminate\Http\Response;
use App\Models\Applications;

class StatisticsTest extends TestCase
{
    /**
     * @var string
     */
    const URL = '/api/v1/logs';

    /**
     * Set up test case
     */
    public function setUp()
    {
        parent::setUp();
        $this->languageDataProvider();
        $this->screensizeDataProvider();
        $this->applicationDataProvider();
        $this->companyDataProvider();
        $this->creativeDataProvider();
        $this->bannerDataProvider();
    }

    /**
     * Testing empty response
     * @return void
     */
    public function testEmptyResponse()
    {
        $response = $this->call('PUT', self::URL);
        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(['status', 'errors', 'errors' => [
                'bundle_id',
                'platform',
                'banner_id',
                'action'
            ]]);
    }

    /**
     * Testing validation response
     * @return void
     */
    public function testValidationResponse()
    {
        $data = [
            'bundle_id' => $this->_faker->text(1024),
            'platform' => $this->_faker->word(),
            'banner_id' => $this->_faker->word(),
            'action' => $this->_faker->word(),
        ];
        $response = $this->call('PUT', self::URL, $data);
        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(['status', 'errors', 'errors' => [
                'bundle_id',
                'platform',
                'banner_id',
                'action'
            ]]);
    }

    /**
     * Testing not found application by bundle_id
     * @return void
     */
    public function testNotFoundApplicationResponse()
    {
        $data = [
            'bundle_id' => md5(time()),
            'platform' => $this->_faker->randomElement(['android', 'ios']),
            'banner_id' => $this->_banner->id,
            'action' => $this->_faker->randomElement(['view', 'viewed', 'click', 'redirect']),
        ];
        $response = $this->call('PUT', self::URL, $data);
        $response->assertStatus(Response::HTTP_NOT_FOUND)
            ->assertJsonStructure(['status', 'errors' => ['application']]);
    }

    /**
     * Testing not found banner by banner_id
     * @return void
     */
    public function testNotFoundBannerResponse()
    {
        $platform = $this->_faker->randomElement(['android', 'ios']);
        $data = [
            'bundle_id' => $platform == 'ios' ? $this->_application->bundle_id_ios : $this->_application->bundle_id_android,
            'platform' => $platform,
            'banner_id' => $this->_faker->randomNumber(),
            'action' => $this->_faker->randomElement(['view', 'viewed', 'click', 'redirect']),
        ];
        $response = $this->call('PUT', self::URL, $data);
        $response->assertStatus(Response::HTTP_NOT_FOUND)
            ->assertJsonStructure(['status', 'errors' => ['banner']]);
    }

    /**
     * Testing success view
     * @return void
     */
    public function testSuccessViewResponse()
    {
        $platform = $this->_faker->randomElement(['android', 'ios']);
        $data = [
            'bundle_id' => $platform == 'ios' ? $this->_application->bundle_id_ios : $this->_application->bundle_id_android,
            'platform' => $platform,
            'banner_id' => $this->_banner->id,
            'action' => 'view',
        ];
        $response = $this->call('PUT', self::URL, $data);
        $response->assertStatus(Response::HTTP_OK)
            ->assertExactJson(['status' => true]);
    }

    /**
     * Testing success viewed
     * @return void
     */
    public function testSuccessViewedResponse()
    {
        $platform = $this->_faker->randomElement(['android', 'ios']);
        $data = [
            'bundle_id' => $platform == 'ios' ? $this->_application->bundle_id_ios : $this->_application->bundle_id_android,
            'platform' => $platform,
            'banner_id' => $this->_banner->id,
            'action' => 'viewed',
        ];
        $response = $this->call('PUT', self::URL, $data);
        $response->assertStatus(Response::HTTP_OK)
            ->assertExactJson(['status' => true]);
    }

    /**
     * Testing success click
     * @return void
     */
    public function testSuccessClickResponse()
    {
        $platform = $this->_faker->randomElement(['android', 'ios']);
        $data = [
            'bundle_id' => $platform == 'ios' ? $this->_application->bundle_id_ios : $this->_application->bundle_id_android,
            'platform' => $platform,
            'banner_id' => $this->_banner->id,
            'action' => 'click',
        ];
        $response = $this->call('PUT', self::URL, $data);
        $response->assertStatus(Response::HTTP_OK)
            ->assertExactJson(['status' => true]);
    }

    /**
     * Testing success redirect
     * @return void
     */
    public function testSuccessRedirectResponse()
    {
        $platform = $this->_faker->randomElement(['android', 'ios']);
        $data = [
            'bundle_id' => $platform == 'ios' ? $this->_application->bundle_id_ios : $this->_application->bundle_id_android,
            'platform' => $platform,
            'banner_id' => $this->_banner->id,
            'action' => 'redirect',
        ];
        $response = $this->call('PUT', self::URL, $data);
        $response->assertStatus(Response::HTTP_OK)
            ->assertExactJson(['status' => true]);
    }
}