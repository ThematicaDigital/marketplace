<?php
Route::get('', ['as' => 'admin.dashboard', function () {
	return redirect('/admin/statistics');
}]);

Route::any('/companies/{id}/copy', [
    'uses' => '\App\Http\Controllers\AdminController@copyCompany',
]);

Route::any('/banners/{id}/copy', [
    'uses' => '\App\Http\Controllers\AdminController@copyBanner',
]);

Route::post('/banners/links', [
    'uses' => '\App\Http\Controllers\AdminController@getLinksForBanner',
]);

Route::post('/statistics/export', [
    'uses' => '\App\Http\Controllers\AdminController@exportStatisticsToCSV',
]);

Route::get('/statistics/banner', [
	'uses' => '\App\Http\Controllers\AdminController@getPerBannerStatistics',
]);

Route::any('/companies/attach-creatives', [
    'uses' => '\App\Http\Controllers\AdminController@attachCreatives',
]);

Route::any('/creatives/{creative_id}/{company_id}/detach', [
    'uses' => '\App\Http\Controllers\AdminController@detachCreatives',
]);

Route::get('/generator', function() {
    Meta::addCss('admin-custom-css-generator', asset('packages/sleepingowl/generator.css'));
    Meta::addJs('admin-custom-js-generator', asset('packages/sleepingowl/generator.js'), 'admin-default');

    $screensizes = \App\Models\Screensizes::get()->mapWithKeys(function ($item) {
	    return [$item->width . 'x' . $item->height => $item->width . 'x' . $item->height];
	});
    $applications = \App\Models\Applications::get();

    return AdminSection::view(
    	View::make(
    		'admin.generator',
    		compact('screensizes', 'applications')
		),
    	'Генератор ссылок'
	);
});