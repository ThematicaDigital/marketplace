<?php

use SleepingOwl\Admin\Navigation\Page;

// Default check access logic
// AdminNavigation::setAccessLogic(function(Page $page) {
// 	   return auth()->user()->isSuperAdmin();
// });
//
// AdminNavigation::addPage(\App\User::class)->setTitle('test')->setPages(function(Page $page) {
// 	  $page
//		  ->addPage()
//	  	  ->setTitle('Dashboard')
//		  ->setUrl(route('admin.dashboard'))
//		  ->setPriority(100);
//
//	  $page->addPage(\App\User::class);
// });
//
// // or
//
// AdminSection::addMenuPage(\App\User::class)
return [

    // [
    //     'title' => 'Локализация',
    //     'icon'  => 'fa fa-globe',
    //     'url'   => 'admin/languages',
    // ],

    // [
    //     'title' => 'Размеры экранов',
    //     'icon'  => 'fa fa-expand',
    //     'url'   => 'admin/screensizes',
    // ],

    // [
    //     'title' => 'Приложения',
    //     'icon'  => 'fa fa-android',
    //     'url'   => 'admin/applications',
    // ],

    // [
    //     'title' => 'Компании',
    //     'icon'  => 'fa fa-list',
    //     'url'   => 'admin/companies',
    // ],

    // [
    //     'title' => 'Креативы',
    //     'icon'  => 'fa fa-picture-o',
    //     'url'   => 'admin/creatives',
    // ],

    // [
    //     'title' => 'Баннера/Видео',
    //     'icon'  => 'fa fa-picture-o',
    //     'url'   => 'admin/banners',
    // ],

    [
        'title' => 'Генератор ссылок',
        'icon'  => 'fa fa-link',
        'url'   => 'admin/generator',
        'priority' => 999
    ],

    [
        'title' => 'Выйти',
        'icon'  => 'fa fa-sign-out',
        'url'   => 'logout',
        'priority' => 1000
    ],

    // Examples
    // [
    //    'title' => 'Content',
    //    'pages' => [
    //
    //        \App\User::class,
    //
    //        // or
    //
    //        (new Page(\App\User::class))
    //            ->setPriority(100)
    //            ->setIcon('fa fa-user')
    //            ->setUrl('users')
    //            ->setAccessLogic(function (Page $page) {
    //                return auth()->user()->isSuperAdmin();
    //            }),
    //
    //        // or
    //
    //        new Page([
    //            'title'    => 'News',
    //            'priority' => 200,
    //            'model'    => \App\News::class
    //        ]),
    //
    //        // or
    //        (new Page(/* ... */))->setPages(function (Page $page) {
    //            $page->addPage([
    //                'title'    => 'Blog',
    //                'priority' => 100,
    //                'model'    => \App\Blog::class
	//		      ));
    //
	//		      $page->addPage(\App\Blog::class);
    //	      }),
    //
    //        // or
    //
    //        [
    //            'title'       => 'News',
    //            'priority'    => 300,
    //            'accessLogic' => function ($page) {
    //                return $page->isActive();
    //		      },
    //            'pages'       => [
    //
    //                // ...
    //
    //            ]
    //        ]
    //    ]
    // ]
];