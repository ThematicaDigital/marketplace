<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;
use Meta;


class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\Models\Banners::class => 'App\Sections\Banners',
        \App\Models\Companies::class => 'App\Sections\Companies',
        \App\Models\Creatives::class => 'App\Sections\Creatives',
        \App\Models\Languages::class => 'App\Sections\Languages',
        \App\Models\Screensizes::class => 'App\Sections\Screensizes',
        \App\Models\Applications::class => 'App\Sections\Applications',
        \App\Models\Statistics::class => 'App\Sections\Statistics',
        \App\Models\Users::class => 'App\Sections\Users',
        \App\Models\Tags::class => 'App\Sections\Tags'
    ];

    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
        parent::boot($admin);
    }
}
