<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Statistics;
use App\Models\Applications;
use App\Models\Companies;
use App\Models\Banners;
use App\Models\Creatives;

class AdminController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function exportStatisticsToCSV(Request $request)
    {
        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=statistics.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];

        \DB::statement('SET sql_mode = ""');

        $query_one = $query_two = Statistics::selectRaw('statistics.*, SUM(conversion) AS conversion_total, SUM(click) AS click_total, SUM(view) AS view_total, SUM(statistics.viewed) AS viewed_total')
            ->leftJoin('applications', 'statistics.application_id', '=', 'applications.id')
            ->leftJoin('companies', 'statistics.company_id', '=', 'companies.id')
            ->leftJoin('creatives', 'statistics.creative_id', '=', 'creatives.id');

        if ($request->ids) {

            $query_one->whereIn('statistics.id', $request->ids);

            foreach ($query_one->get() as $statistics) {

                $query_two->orWhere(function ($query) use ($statistics) {
                    $query->where('application_id', $statistics->application_id);
                    $query->where('creative_id', $statistics->creative_id);
                    $query->where('company_id', $statistics->company_id);
                    $query->where('date', $statistics->date);
                    $query->where('statistics.platform', $statistics->platform);
                });

            }

        } else {

            if ($request->application_title)
                $query_one->where('applications.title', 'like', "%$request->application_title%");

            if ($request->date_from)
                $query_one->whereDate('date', '>=', Carbon::createFromFormat('d-m-Y', $request->date_from)->toDateString());

            if ($request->date_to)
                $query_one->whereDate('date', '<=', Carbon::createFromFormat('d-m-Y', $request->date_to)->toDateString());

            if ($request->get('platform', 'Both') != 'Both')
                $query_one->where('statistics.platform', Applications::encodePlatform($request->platform));

            if ($request->company_title) {
                $query_one->where(function ($query) use ($request) {
                    $query->where('companies.title', 'like', "%$request->company_title%")
                        ->orWhere('applications.title', 'like', "%$request->company_title%");
                });
            }

            if ($request->creative_title)
                $query_one->where('creatives.title', 'like', "%$request->creative_title%");

            if ($request->tag_name) {
                $query_one->whereIn('statistics.company_id', function ($query) use ($request) {
                    $query->select('company_id')
                        ->distinct()
                        ->from('tags_companies_pivot')
                        ->leftJoin('tags', 'tags_companies_pivot.tag_id', '=', 'tags.id')
                        ->where('tags.name', 'like', "%$request->tag_name%");
                });
            }

            if ($request->conversion_total_from)
                $query_one->having('conversion_total', '>=', $request->conversion_total_from);

            if ($request->conversion_total_to)
                $query_one->having('conversion_total', '<=', $request->conversion_total_to);

            if ($request->click_total_from)
                $query_one->having('click_total', '>=', $request->click_total_from);

            if ($request->click_total_to)
                $query_one->having('click_total', '<=', $request->click_total_to);

            if ($request->ctr_from)
                $query_one->havingRaw('ROUND((click_total / view_total) * 100) >= ' . intval($request->ctr_from));

            if ($request->ctr_to)
                $query_one->havingRaw('ROUND((click_total / view_total) * 100) <= ' . intval($request->ctr_to));

            if ($request->view_total_from)
                $query_one->having('view_total', '>=', $request->view_total_from);

            if ($request->view_total_to)
                $query_one->having('view_total', '<=', $request->view_total_to);

            if ($request->viewed_total_from)
                $query_one->having('viewed_total', '>=', $request->viewed_total_from);

            if ($request->viewed_total_to)
                $query_one->having('viewed_total', '<=', $request->viewed_total_to);

        }

        $statistics = $query_one->groupBy('application_id', 'creative_id', 'company_id', 'date', 'platform')->get();

        $columns = [
            'ID',
            'Приложение',
            'Дата',
            'Платформа',
            'Компания',
            'Креатив',
            'Переходы',
            'Клики',
            'CTR',
            'Показы',
            'Просмотры',
        ];

        $callback = function() use ($statistics, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            foreach($statistics as $statistic) {
                fputcsv(
                    $file,
                    [
                        $statistic->id,
                        $statistic->application_title,
                        $statistic->date,
                        ($statistic->platform == Applications::PLATFORM_ANDROID) ? 'Android' : 'iOS',
                        "{$statistic->application_title} > {$statistic->company_title}",
                        $statistic->creative_title,
                        $statistic->conversion_total,
                        $statistic->click_total,
                        ($statistic->view_total > 0) ? round((($statistic->click_total/$statistic->view_total))*100) . "%" : '-',
                        $statistic->view_total,
                        $statistic->viewed_total,
                    ]
                );
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    /**
     * Get redirect link for a banners
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getLinksForBanner(Request $request)
    {
        $creative_id = $request->creative_id;

        $creative = Creatives::where('id', $creative_id)->first();

        $json = ['status' => false];
        if ($creative && $creative->company) {
            if (!empty($creative->company) && !empty($creative->company->start_date)) {
                $medium = date('Ymd', strtotime($creative->company->start_date)) . '_branding_' . $creative_id;
            } else {
                $medium = 'branding_' . $creative_id;
            }
            $source = 'ingame';
            $campaign = 'mobileads';

            if ($creative->company->application->type == Applications::TYPE_WEB) {
                $json = [
                    'status' => true,
                    'links' => [
                        'web' => "http://i-mult.ru/?utm_source={$source}&utm_campaign={$campaign}&utm_medium={$medium}&utm_term=redirect"
                    ]
                ];
            } elseif ($creative->company->application->type == Applications::TYPE_MOBILE) {
                if ($creative->company->application->platform == Applications::PLATFORM_ANDROID) {
                    $json = [
                        'status' => true,
                        'links' => [
                            'android' => "https://play.google.com/store/apps/details?id={$creative->company->application->bundle_id}&referrer=utm_source%3D{$source}%26utm_medium%3D{$medium}%26utm_campaign%3D{$campaign}"
                        ]
                    ];
                } elseif ($creative->company->application->platform == Applications::PLATFORM_IOS) {
                    $json = [
                        'status' => true,
                        'links' => [
                            'ios' => "https://itunes.apple.com/app/{$creative->company->application->bundle_id}?pt=118235511&ct={$source}_{$campaign}_{$medium}&mt=8"
                        ]
                    ];
                }
            }
        }
        return response()->json($json, Response::HTTP_OK);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function copyCompany($id)
    {
        $company = Companies::find($id);

        if (!$company) {
            return redirect('/admin/companies/');
        }

        $newCompany = $company->toArray();
        $newCompany['title'] .= ' (копия)';

        Companies::create($newCompany);

        return redirect('/admin/companies/');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function copyBanner($id)
    {
        $banner = Banners::find($id);

        if (!$banner) {
            return redirect('/admin/banners/');
        }

        $newBanner = $banner->toArray();

        $storage = public_path();

        $uniqid = md5(uniqid());

        $extension = \File::extension("{$storage}/{$newBanner['image']}");

        if (!empty($newBanner['image'])) {
            if (\File::exists("{$storage}/{$newBanner['image']}")) {
                \File::copy(
                    "{$storage}/{$newBanner['image']}",
                    public_path(config('sleeping_owl.filesUploadDirectory')). "/{$uniqid}.{$extension}"
                );
                $newBanner['image'] = config('sleeping_owl.filesUploadDirectory'). "/{$uniqid}.{$extension}";
            }
        }

        if (!empty($newBanner['video'])) {
            if (\File::exists("{$storage}/{$newBanner['video']}")) {
                \File::copy(
                    "{$storage}/{$newBanner['video']}",
                    public_path(config('sleeping_owl.filesUploadDirectory')). "/{$uniqid}.{$extension}"
                );
                $newBanner['video'] = config('sleeping_owl.filesUploadDirectory'). "/{$uniqid}.{$extension}";
            }
        }

        Banners::create($newBanner);

        return back();
    }

    /**
     * Attach creatives for company
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function attachCreatives(Request $request)
    {
        $company_id = $request->company_id;
        $creative_id = $request->creatives;

        $creative = \App\Models\Creatives::find($creative_id);
        if (!$creative->companies()->where('company_id', $company_id)->exists()) {
            $creative->companies()->attach([$company_id]);
            return response()->json(['status' => true], Response::HTTP_OK);
        }

        return response()->json(['status' => false], Response::HTTP_OK);
    }

    /**
     * Detach creatives for company
     *
     * @param int $creative_id
     * @param int $company_id
     *
     * @return \Illuminate\Http\Response
     */
    public function detachCreatives($creative_id, $company_id)
    {
        $creative = \App\Models\Creatives::find($creative_id);
        if ($creative->companies()->where('company_id', $company_id)->exists()) {
            $creative->companies()->detach([$company_id]);
        }

        return redirect(request()->headers->get('referer'));
    }

    public function getPerBannerStatistics(Request $request)
    {
        $ids = $request->get('ids', false);

        $query_one = $query_two = Statistics::selectRaw('statistics.*')
            ->leftJoin('applications', 'statistics.application_id', '=', 'applications.id')
            ->leftJoin('companies', 'statistics.company_id', '=', 'companies.id')
            ->leftJoin('creatives', 'statistics.creative_id', '=', 'creatives.id')
            ->whereNotNull('statistics.banner_id');

        if ($ids)
            $query_one->whereIn('statistics.id', $ids);

        foreach ($query_one->get() as $statistics) {

            $query_two->orWhere(function ($query) use ($statistics) {
                $query->where('application_id', $statistics->application_id);
                $query->where('creative_id', $statistics->creative_id);
                $query->where('company_id', $statistics->company_id);
                $query->where('date', $statistics->date);
                $query->where('statistics.platform', $statistics->platform);
            });

        }

        return response()->json($query_two->get());
    }
}