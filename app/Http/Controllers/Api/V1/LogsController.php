<?php
namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller as Controller;
use Validator;

use \App\Models\Banners;
use \App\Models\Languages;
use \App\Models\Screensizes;
use \App\Models\Creatives;
use \App\Models\Applications;
use \App\Models\Statistics;
use \App\Models\Companies;

class LogsController extends Controller
{
    const ACTION_VIEW = 'view';

	const ACTION_VIEWED = 'viewed';

	const ACTION_CLICK = 'click';

	const ACTION_REDIRECT = 'redirect';

	/**
	 * Write logs
	 *
	 * @api {put} /logs Write logs
	 * @apiDescription Request to write to log of user's action.<br/><br/>
	 * Actions:
     * - view - when banner was started
	 * - viewed - when banner was viewed before the end
	 * - click - when user clicked on a banner
	 * - redirect - when user was redirected to banner's link
	 * @apiName SetLog
	 * @apiGroup Logs
	 * @apiVersion 1.0.0
     * @apiPermission none
	 *
	 * @apiParam {String{..64}} bundle_id Bundle ID for application
	 * @apiParam {String{2..8}="ios","android"} platform Device's platform
	 * @apiParam {Number{1..}} banner_id Banner ID
	 * @apiParam {String{4..8}="view", "viewed", "click","redirect"} action User's action
	 *
	 * @apiSuccess (200) {Boolean} status Status request
 	 *
 	 * @apiError {Boolean} status Status request
 	 * @apiError {String[]} [errors] Error messages
     *
     * @apiSampleRequest http://45.77.43.157/api/v1/logs
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *         "bundle_id": "i.mult.parovozov",
     *         "platform": "android",
     *         "banner_id": "1",
     *         "action": "click"
     *     }
	 *
	 * @apiSuccessExample {json} Success-Response:
	 * 	HTTP/1.1 200 OK
 	 *      {
 	 *      	"status": true
 	 *    	}
 	 *
 	 * @apiErrorExample {json} Error-Response:
	 * 	HTTP/1.1 400 OK
 	 *     	{
 	 *      	"status": false,
 	 *       	"errors": {
	 *	    		"bundle_id": [
	 *		      		"Поле bundle id обязательно для заполнения."
	 *		    	],
	 *	    	}
	 *	    }
 	 */
    public function put(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'bundle_id' => 'required|max:64',
            'platform' => 'required|in:ios,android',
            'banner_id' => 'required|integer',
            'action' => 'required|in:view,viewed,click,redirect',
        ]);

        if ($validator->fails()) {
        	return response()->json(['status' => false, 'errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        }

        $platform = Applications::encodePlatform($request->platform);
        if (!$platform) {
        	return response()->json(['status' => false, 'errors' => ['platform' => 'Platform not found']], Response::HTTP_NOT_FOUND);
        }

        if ($platform == Applications::PLATFORM_ANDROID) {
            $application = Applications::where('bundle_id_android', $request->bundle_id)->first();
        } else {
            $application = Applications::where('bundle_id_ios', $request->bundle_id)->first();
        }

        if (!$application) {
        	return response()->json(['status' => false, 'errors' => ['application' => ['Application not found']]], Response::HTTP_NOT_FOUND);
        }

        $banner = Banners::where('id', $request->banner_id)->first();
        if (!$banner) {
        	return response()->json(['status' => false, 'errors' => ['banner' => ['Banner not found']]], Response::HTTP_NOT_FOUND);
        }

        $company = Companies::leftJoin('applications_companies_pivot', 'applications_companies_pivot.company_id', '=', 'companies.id')
            ->leftJoin('companies_creatives_pivot', 'companies_creatives_pivot.company_id', '=', 'companies.id')
            ->where('applications_companies_pivot.application_id', $application->id)
            ->where('companies_creatives_pivot.creative_id', $banner->creative->id)
            ->first();

        $statistics = new Statistics;

        $status = false;
        switch ($request->action) {
            case self::ACTION_VIEW:
                $status = $statistics->setView($application, $banner, $company, $request->platform);
                break;
        	case self::ACTION_VIEWED:
        		$status = $statistics->setViewed($application, $banner, $company, $request->platform);

                $company->viewed += 1;
                $company->save();

        		break;
        	case self::ACTION_CLICK:
        		$status = $statistics->setClick($application, $banner, $company, $request->platform);
        		break;
        	case self::ACTION_REDIRECT:
        		$status = $statistics->setRedirect($application, $banner, $company, $request->platform);
        		break;
        }

    	return response()->json(['status' => $status], Response::HTTP_OK);
    }
}