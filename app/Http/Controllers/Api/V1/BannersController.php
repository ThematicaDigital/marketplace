<?php
namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Facades\DB;
use Request as HttpRequest;

use \App\Models\Banners;
use \App\Models\Languages;
use \App\Models\Screensizes;
use \App\Models\Creatives;
use \App\Models\Applications;
use \App\Models\Companies;
use Validator;
use Carbon\Carbon;


/**
 * @resource Banners
 *
 * Banners
 */
class BannersController extends Controller
{
	/**
	 * Get all banners
	 *
	 * @api {get} /banners Get banners
	 * @apiDescription Request information about banners/videos
	 * @apiName GetBanners
	 * @apiGroup Banners
	 * @apiVersion 1.0.2
     * @apiPermission none
	 *
	 * @apiParam {String{..64}} bundle_id Bundle ID of application
	 * @apiParam {String{2..8}="ios","android"} platform Device's platform
	 * @apiParam {String{2}} locale Device's locale
	 * @apiParam {String{4..12}} resolution Device's screen resolution
	 * @apiParam {String{4..8}="wifi","mobile"} connection_type Device's type connection
	 *
	 * @apiSuccess (200) {Boolean} status Status request
	 * @apiSuccess (200) {Number} count Count of banners
     * @apiSuccess (200) {Object[]} banners List of user banners information
     * @apiSuccess (200) {Number} banners.id Banner ID
     * @apiSuccess (200) {String} banners.type Type of banner ("video" or "banner")
     * @apiSuccess (200) {String} banners.image Link of image for banner
     * @apiSuccess (200) {String} banners.video Link of video for banner
	 * @apiSuccess (200) {String} banners.link Link of redirect for banner
	 * @apiSuccess (200) {Number} banners.campaign_id Campaign ID
	 * @apiSuccess (200) {Object[]} campaigns List of campaigns
	 * @apiSuccess (200) {Number} campaigns.id Campaign ID
	 * @apiSuccess (200) {Number{1-5}} campaigns.priority Campaign priority
 	 *
 	 * @apiError {Boolean} status Status request
 	 * @apiError {String[]} [errors] Error messages
     *
     * @apiSampleRequest http://45.77.43.157/api/v1/banners
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *         "bundle_id": "i.mult.parovozov",
     *         "platform": "android",
     *         "locale": "en",
     *         "resolution": "750x1560",
     *         "connection_type": "wifi"
     *     }
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *		HTTP/1.1 200 OK
 	 *      {
     *          "status": true,
 	 *      	"count": 2,
 	 *      	"banners": [
     *              {
     *                  "id": 1,
     *                  "type": "banner",
     *                  "image": "files/uploads/900x1600.jpg",
     *                  "video": null,
     *                  "link": "https://play.google.com/store/apps/details?id=1",
	 * 					"campaign_id": 3
     *              },
     *              {
     *                  "id": 2,
     *                  "type": "video",
     *                  "image": "files/uploads/750x1560.jpg",
     *                  "video": "files/uploads/750x1560.avi",
     *                  "link": "https://play.google.com/store/apps/details?id=1",
	 *					"campaign_id": 4
     *              }
     *          ],
     *			"campaigns": [
	 * 				{
     *					"id": 3,
	 * 					"priority": 1
	 * 				},
	 * 				{
	 *					"id": 4,
	 * 					"priority": 5
	 * 				}
	 * 			]
 	 *    	}
 	 *
 	 * @apiErrorExample {json} Error-Response:
	 * 		HTTP/1.1 400 OK
 	 *     	{
 	 *       	"status": false,
 	 *       	"errors": {
	 *	    		"bundle_id": [
	 *		      		"Поле bundle id обязательно для заполнения."
	 *		    	],
	 *	    	}
	 *	    }
 	 */
    public function get(Request $request)
    {
        $locales_list = Languages::all()->pluck('title')->all();

    	$validator = Validator::make($request->all(), [
            'bundle_id' => 'required|max:64',
            'platform' => 'required|in:ios,android|max:8',
            'locale' => 'nullable|in:' . implode(',', $locales_list) . '|max:2',
            'resolution' => 'regex:/^(\d+)x(\d+)/|required|max:12',
            'connection_type' => 'required|in:wifi,mobile',
        ]);

        if ($validator->fails()) {
        	return response()->json(['status' => false, 'errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        }

        $platform = Applications::encodePlatform($request->platform);
        if (!$platform) {
        	return response()->json(['status' => false, 'errors' => ['platform' => 'Platform not found']], Response::HTTP_NOT_FOUND);
        }

        if ($platform == Applications::PLATFORM_ANDROID) {
            $application = Applications::where('bundle_id_android', $request->bundle_id)->first();
        } else {
            $application = Applications::where('bundle_id_ios', $request->bundle_id)->first();
        }

        if (!$application) {
        	return response()->json(['status' => false, 'errors' => ['application' => ['Application not found']]], Response::HTTP_NOT_FOUND);
        }

        $language = Languages::where('title', $request->locale)->first();
        if (!$language) {
        	return response()->json(['status' => false, 'errors' => ['language' => ['Locale not found']]], Response::HTTP_NOT_FOUND);
        }

        if (HttpRequest::header('If-Modified-Since')) {

			$date = Carbon::parse(HttpRequest::header('If-Modified-Since'))->setTimezone(date_default_timezone_get());
			$tables = ['applications', 'companies', 'creatives', 'banners'];

			$count = array_sum(array_map(function ($table) use ($date) {
				return DB::table($table)->where('updated_at', '>', $date)->count();
			}, $tables));

			if ($count == 0 && $date->diffInHours(Carbon::now()) < 1)
        		return response()->json(['status' => true], Response::HTTP_NOT_MODIFIED);
        }

        list($width, $height) = explode('x', $request->resolution);
        $orientation = ($width > $height) ? Screensizes::LANDSCAPE : Screensizes::PORTRAIT;
        $ratio = Banners::ratio($width, $height);

		if (env('DB_CONNECTION') == 'mysql')
        	DB::statement('SET sql_mode = ""');

        $banners = Banners::select(
                'banners.id',
                'banners.type',
                'banners.image',
                'banners.video',
                'banners.creative_id',
                'banners.link',
				'companies.id AS company_id',
				'companies.priority AS company_priority',
                'screensizes.width',
                'screensizes.height'
            )
            ->where('language_id', $language->id)
            ->leftJoin('creatives', 'banners.creative_id', '=', 'creatives.id')
        	->leftJoin('companies_creatives_pivot', 'banners.creative_id', '=', 'companies_creatives_pivot.creative_id')
            ->leftJoin('companies', 'companies_creatives_pivot.company_id', '=', 'companies.id')
            ->leftJoin('applications_companies_pivot', 'applications_companies_pivot.company_id', '=', 'companies.id')
            ->leftJoin('screensizes', 'banners.screensize_id', '=', 'screensizes.id')
        	->where('applications_companies_pivot.application_id', $application->id)
            ->where('screensizes.orientation', $orientation)
        	->where('companies.start_date', '<=', Carbon::now()->toDateString())
            ->where(function ($query) {
                $query->whereNull('companies.end_date')
                    ->orWhere('companies.end_date', '>', Carbon::now()->toDateString());
            })
            ->where(function ($query) use ($platform) {
                $query->whereNull('companies.platform')
                    ->orWhere('companies.platform', $platform);
            })
            ->whereNotNull('companies.id')
            ->whereNotNull('banners.image')
            ->where('companies.active', true)
            ->whereNull('companies.deleted_at')
            ->whereNull('creatives.deleted_at')
			->where(function ($query) {
				$query->whereRaw('companies.viewed < companies.limit_views')
					->orWhereNull('companies.limit_views')
					->orWhere('companies.limit_views', 0);
			})
        	->whereIn('companies.connection_type', [
        		Companies::encodeConnectionType($request->connection_type),
        		Companies::CONNECTION_BOTH
    		])
            ->get();

        $widthBanners = ($orientation == Screensizes::LANDSCAPE) ? $banners->keyBy('width') : $banners->keyBy('height');
        $maxWidth = $banners->max('width');
        $maxHeigh = $banners->max('height');

        if (!empty($banners) && count($banners) > 0) {
            // Grouping banners by aspect ratio
            $groups = [];
            foreach ($banners as $i => $banner) {
                $groups[Banners::ratio($banner->width, $banner->height)][] = $banner;
            }

            // Return group if has aspect ratio
            if (!empty($groups[$ratio]) && count($groups[$ratio]) > 0) {
                $group = $groups[$ratio];
            // Find and return group if not has aspect ratio
            } else {
                if ($orientation == Screensizes::LANDSCAPE) {
                    $maxRatio = Banners::ratio($widthBanners[$maxWidth]->width, $widthBanners[$maxWidth]->height);
                } else {
                    $maxRatio = Banners::ratio($widthBanners[$maxHeigh]->width, $widthBanners[$maxHeigh]->height);
                }
                $group = $groups[$maxRatio];
            }

            // Format the response JSON
            $json = [];
            foreach ($group as $i => $banner) {
                $json[0][$i + 1] = [
                    'id' => $banner->id,
                    'type' => ($banner->type == Banners::TYPE_BANNER) ? 'banner' : 'video',
                    'image' => !empty($banner->image) ? cdn($banner->image) : null,
                    'video' => !empty($banner->video) ? cdn($banner->video) : null,
                    'link' => $banner->link,
                    'campaign_id' => $banner->company_id,
                ];
                $json[1][$banner->company_id] = [
                    'id' => $banner->company_id,
                    'priority' => $banner->company_priority
                ];
            }
            return response()
                ->json([
                    'status' => true,
                    'count' => count($json[0]),
                    'banners' => $json[0],
                    'campaigns' => array_values($json[1])
                ], Response::HTTP_OK, [], JSON_OBJECT_AS_ARRAY)
                ->header('Last-Modified', substr(gmdate('r', time()), 0, -5) . 'GMT');
        }

        return response()
                ->json([
                    'status' => true,
                    'count' => 0,
                    'banners' => new \stdClass(),
                    'campaigns' => []
                ], Response::HTTP_OK, [], JSON_OBJECT_AS_ARRAY);
    }
}