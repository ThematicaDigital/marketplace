<?php
namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Request as HttpRequest;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller as Controller;
use Validator;

use \App\Models\Applications;
use \App\Http\Requests\Frequence;

class FrequenceController extends Controller
{
	/**
	 * Get frequence
	 *
	 * @api {get} /frequence Get frequence
	 * @apiDescription Request settings for the frequency of displaying banners/videos
	 * @apiName GetFrequences
	 * @apiGroup Frequences
	 * @apiVersion 1.0.0
     * @apiPermission none
	 *
	 * @apiParam {String{..64}} bundle_id Bundle ID for application
	 * @apiParam {String{2..8}="ios","android"} platform Device's platform
	 *
	 * @apiSuccess (200) {Boolean} status Status request
	 * @apiSuccess (200) {Number} frequence_banner_time Interval in minutes between previews of banners in the application
	 * @apiSuccess (200) {Number} frequence_banner_session Maximum number of banner previews per session
	 * @apiSuccess (200) {Number} frequence_video_time Interval in minutes between previews of videos in the application
	 * @apiSuccess (200) {Number} frequence_video_session Maximum number of video previews per session
	 * @apiSuccess (200) {Number} frequence_other_time Interval in minutes between previews of other types in the application
	 * @apiSuccess (200) {Number} frequence_other_session Maximum number of other type previews per session
 	 *
 	 * @apiError {Boolean} status Status request
 	 * @apiError {String[]} [errors] Error messages
     *
     * @apiSampleRequest http://45.77.43.157/api/v1/frequence
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *         "bundle_id": "i.mult.parovozov",
     *         "platform": "android"
     *     }
	 *
	 * @apiSuccessExample {json} Success-Response:
	 * 	HTTP/1.1 200 OK
 	 *      {
 	 *      	"status": true,
 	 *      	"frequence_banner_time": 1,
 	 *      	"frequence_banner_session": 2,
 	 *      	"frequence_video_time": 3,
 	 *      	"frequence_video_session": 4,
 	 *      	"frequence_other_time": 5,
 	 *      	"frequence_other_session": 6,
 	 *    	}
 	 *
 	 * @apiErrorExample {json} Error-Response:
	 * 	HTTP/1.1 400 OK
 	 *     	{
 	 *      	"status": false,
 	 *       	"errors": {
	 *	    		"bundle_id": [
	 *		      		"Поле bundle id обязательно для заполнения."
	 *		    	],
	 *	    	}
	 *	    }
 	 */
    public function get(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'bundle_id' => 'required|string|max:64',
            'platform' => 'required|in:ios,android'
        ]);

        if ($validator->fails()) {
        	return response()->json(['status' => false, 'errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        }

        if (HttpRequest::header('If-Modified-Since')) {
        	$count = Applications::where('updated_at', '>', date('Y-m-d H:i:s', strtotime(HttpRequest::header('If-Modified-Since'))))->count();
        	if ($count == 0) {
        		return response()
	        		->json(['status' => true], Response::HTTP_NOT_MODIFIED)
	        		->header('Last-Modified', substr(gmdate('r', time()), 0, -5) . 'GMT');
        	}
        }

        $platform = Applications::encodePlatform($request->platform);
        if (!$platform) {
        	return response()->json(['status' => false, 'errors' => ['platform' => 'Platform not found']], Response::HTTP_NOT_FOUND);
        }

        if ($platform == Applications::PLATFORM_ANDROID) {
        	$application = Applications::where('bundle_id_android', $request->bundle_id)->first();
        } else {
        	$application = Applications::where('bundle_id_ios', $request->bundle_id)->first();
        }

        if (!$application) {
        	return response()->json(['status' => false, 'errors' => ['application' => ['Application not found']]], Response::HTTP_NOT_FOUND);
        }

    	$json = [
			'status' => true,
			'frequence_banner_time' => $application->frequence_banner_time,
			'frequence_banner_session' => $application->frequence_banner_session,
			'frequence_video_time' => $application->frequence_video_time,
			'frequence_video_session' => $application->frequence_video_session,
			'frequence_other_time' => $application->frequence_other_time,
			'frequence_other_session' => $application->frequence_other_session,
		];

		return response()
			->json($json, Response::HTTP_OK)
			->header('Last-Modified', substr(gmdate('r', time()), 0, -5) . 'GMT');
    }
}