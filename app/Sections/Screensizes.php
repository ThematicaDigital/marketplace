<?php

namespace App\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;

use \App\Models\Users;

use Illuminate\Support\Facades\Auth;

/**
 * Class Screensizes
 *
 * @property \Models/Screensizes $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Screensizes extends Section implements Initializable
{
    /**
     * Model
     * @var string
     */
    protected $model = '\App\Models\Screensizes';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Размеры экранов';

    /**
     * @var string
     */
    protected $alias = 'screensizes';

    /**
     * Initialize section
     */
    public function initialize()
    {

    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setOrder([[0, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('width', 'Ширина'),
                AdminColumn::text('height', 'Высота'),
                AdminColumn::custom('Ориентация', function ($instance) {
                    if ($instance->orientation == \App\Models\Screensizes::LANDSCAPE) {
                        return 'Landscape';
                    } else {
                        return 'Portrait';
                    }
                }),
                AdminColumn::count('banners', 'Кол-во баннеров')
            )->paginate(25);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('width', 'Ширина')->required()->addValidationRule('integer'),
            AdminFormElement::text('height', 'Высота')->required()->addValidationRule('integer'),
            AdminFormElement::select('orientation', 'Ориентация')->setOptions([
                \App\Models\Screensizes::LANDSCAPE => 'Landscape',
                \App\Models\Screensizes::PORTRAIT => 'Portrait',
            ])->setDefaultValue(\App\Models\Screensizes::LANDSCAPE)->required()
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $form = $this->onEdit(null);
        $form->getButtons()->replaceButtons([
            'delete' => null
        ]);

        return  $form;
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
