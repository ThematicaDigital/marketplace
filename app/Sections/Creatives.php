<?php

namespace App\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminSection;
use AdminDisplayFilter;
use Request;

use \App\Models\Users;
use App\Models\Companies;
use App\Models\Applications;

use Illuminate\Support\Facades\Auth;

/**
 * Class Creatives
 *
 * @property \Models/Creatives $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Creatives extends Section implements Initializable
{
    /**
     * Model
     * @var string
     */
    protected $model = '\App\Models\Creatives';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Креативы';

    /**
     * @var string
     */
    protected $alias = 'creatives';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-picture-o')->setPriority(12)->setAccessLogic(function (Page $page) {
            if (Auth::user()->group_id == Users::GROUP_ADMIN) {
                return true;
            }
            return false;
        });
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay($scopes = [])
    {
        $display = AdminDisplay::datatables()
            ->with('banners')
            ->setOrder([[0, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('title', 'Название'),
                AdminColumn::count('banners', 'Кол-во баннеров')->setWidth(200)
            )->paginate(25);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::panel();

        $banners = AdminSection::getModel(\App\Models\Banners::class)->fireDisplay(['params' => ['withCreative' => $id]]);
        $banners->getScopes()->push(['withCreative', $id]);
        $banners->setParameter('creative_id', $id);

        $tabs = AdminDisplay::tabbed([
            'Настройки креатива' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::text('title', 'Название')->required()->addValidationRule('string')
            ]),
            'Баннера/видео креатива' => new \SleepingOwl\Admin\Form\FormElements([
                $banners
            ]),
        ]);
        $form->addElement($tabs);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $form = AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Название')->required()->addValidationRule('string'),
            AdminFormElement::hidden('companies')
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null
        ]);

        return $form;
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
