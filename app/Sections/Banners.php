<?php

namespace App\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Meta;
use Request;

use \App\Models\Screensizes;
use \App\Models\Languages;
use \App\Models\Creatives;
use \App\Models\Users;

use Illuminate\Support\Facades\Auth;

/**
 * Class Banners
 *
 * @property \Models/Banners $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Banners extends Section
{
    /**
     * Model
     * @var string
     */
    protected $model = '\App\Models\Banners';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Баннера/Видео';

    /**
     * @var string
     */
    protected $alias = 'banners';

    /**
     * @return DisplayInterface
     */
    public function onDisplay($params = null)
    {
        $display = AdminDisplay::datatablesAsync()
            ->with('language')
            ->with('creative')
            ->setOrder([[0, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('title', 'Название'),
                AdminColumn::custom('Превью', function ($instance) {
                    if (!empty($instance->image)) {
                        return "<a href='/{$instance->image}' data-toggle='lightbox'><img class='thumbnail' src='/{$instance->image}' width='200px'></a>";
                    } elseif (!empty($instance->video)) {

                    }
                })->setWidth(200),
                AdminColumn::relatedLink('creative.title', 'Креатив'),
                AdminColumn::custom('Тип', function ($instance) {
                    if ($instance->type == \App\Models\Banners::TYPE_BANNER) {
                        return "Баннер";
                    }
                    return "Видео";
                }),
                AdminColumn::custom('Разрешение', function ($instance) {
                    $screenSize = $instance->screensize;
                    $orientation = $screenSize->orientation == Screensizes::LANDSCAPE ? 'Landscape' : 'Portrait';
                    return "{$screenSize->width}x{$screenSize->height} ({$orientation})";
                }),
                AdminColumn::text('language.title', 'Локаль')
            )->paginate(25);

        $control = $display->getColumns()->getControlColumn();

        $button = new \SleepingOwl\Admin\Display\ControlLink(function (\Illuminate\Database\Eloquent\Model $model) {
           return '/admin/banners/' . $model->getKey() . '/copy';
        }, 'Копировать', 50);
        // $control->addButton($button);
        $button->hideText();
        $button->setIcon('fa fa-clone');
        $button->setHtmlAttribute('class', 'btn-success');
        $control->addButton($button)->setWidth('100px');

        if ($params) {
            if (!empty($params['withCreative'])) {
                $display->setScopes(['withCreative' , $params['withCreative']]);
            }
        }

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        Meta::addJs('admin-custom-banners', asset('packages/sleepingowl/banners.js'), 'admin-default');

        if ($id) {
            $banner = \App\Models\Banners::find($id);
            $creative_id = $banner->creative_id;
        } elseif (Request::input('creative_id')) {
            $creative_id = Request::input('creative_id');
        }

        if (!empty($creative_id)) {
            $creative = Creatives::find($creative_id);
        }

        if (!empty($creative) && $creative->companies()->count() > 0) {
            $screenSizes = Screensizes::withCreativeId($creative_id)->get();
            $languages = Languages::withCreativeId($creative_id)->get();
        } else {
            $screenSizes = Screensizes::get();
            $languages = Languages::get();
        }

        $screenSizesOptions = [];
        if ($screenSizes) {
            foreach ($screenSizes as $screenSize) {
                $orientation = $screenSize->orientation == Screensizes::LANDSCAPE ? 'Landscape' : 'Portrait';
                $screenSizesOptions[$screenSize->id] = "{$screenSize->width}x{$screenSize->height} ({$orientation})";
            }
        }

        $languagesOptions = [];
        if ($languages) {
            foreach ($languages as $language) {
                $languagesOptions[$language->id] = $language->title;
            }
        }

        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Название')->required(),
            AdminFormElement::select('creative_id', 'Креатив', Creatives::class, 'title')->required(),
            AdminFormElement::select('type', 'Тип', [
                \App\Models\Banners::TYPE_BANNER => 'Баннер',
                \App\Models\Banners::TYPE_VIDEO => 'Видео',
            ])->setDefaultValue(\App\Models\Banners::TYPE_BANNER)->required(),
            AdminFormElement::select('language_id', 'Локаль')->setOptions($languagesOptions)->required(),
            AdminFormElement::multiselect('screensize_id', 'Разрешение')
                ->setOptions($screenSizesOptions)
                ->taggable()
                ->setHtmlAttribute('data-maximum-selection-length', 1)
                ->required(),
            AdminFormElement::file('video', 'Видео'),
            AdminFormElement::file('image', 'Изображение')->addValidationRule('image'),
            AdminFormElement::text('link', 'Ссылка'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $form = $this->onEdit(null);
        $form->getButtons()->replaceButtons([
            'delete' => null
        ]);

        return  $form;
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
