<?php

namespace App\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminSection;
use Meta;

use \App\Models\Users;
use \App\Models\Languages;
use \App\Models\Screensizes;

use Illuminate\Support\Facades\Auth;

/**
 * Class Applications
 *
 * @property \Models/Applications $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Applications extends Section implements Initializable
{
    /**
     * Model
     * @var string
     */
    protected $model = '\App\Models\Applications';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Приложения';

    /**
     * @var string
     */
    protected $alias = 'applications';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-android')->setPriority(10)->setAccessLogic(function (Page $page) {
            if (Auth::user()->group_id == Users::GROUP_ADMIN) {
                return true;
            }
            return false;
        });
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setOrder([[0, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('title', 'Название'),
                AdminColumn::text('bundle_id_ios', 'Bundle ID iOS'),
                AdminColumn::text('bundle_id_android', 'Bundle ID Android')
            )->paginate(25);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::panel();

        $form->addBody([
            AdminFormElement::columns()
            ->addColumn(function() {
                return [
                    AdminFormElement::text('title', 'Название')->required()->addValidationRule('string'),
                    AdminFormElement::multiselect('languages', 'Локализация', Languages::class)->taggable(),
                    AdminFormElement::multiselect('screensizes', 'Размеры экрана', Screensizes::class)
                        ->taggable()
                        ->setLoadOptionsQueryPreparer(function($element, $query) {
                            return $query->select('id', \DB::raw('CONCAT(width, "x", height, IF(width > height, " (Landscape)", " (Portrait)")) as title'));
                        })
                        ->setDisplay('title'),
                ];
            })->addColumn(function() {
                return [
                    AdminFormElement::text('frequence_banner_time', 'Интервал показа баннера для пользователя (мин.)')
                        ->required()
                        ->addValidationRule('integer')
                        ->setDefaultValue(0),
                    AdminFormElement::text('frequence_banner_session', 'Максимальное кол-во показов баннера за сессию')
                        ->required()
                        ->addValidationRule('integer')
                        ->setDefaultValue(0),
                    AdminFormElement::text('frequence_video_time', 'Интервал показа видео для пользователя (мин.)')
                        ->required()
                        ->addValidationRule('integer')
                        ->setDefaultValue(0),
                    AdminFormElement::text('frequence_video_session', 'Максимальное кол-во показов видео за сессию')
                        ->required()
                        ->addValidationRule('integer')
                        ->setDefaultValue(0),
                    AdminFormElement::text('frequence_other_time', 'Интервал показа из рекламной сети для пользователя (мин.)')
                        ->addValidationRule('integer')
                        ->setDefaultValue(0),
                    AdminFormElement::text('frequence_other_session', 'Максимальное кол-во показов из рекламной сети за сессию')
                        ->addValidationRule('integer')
                        ->setDefaultValue(0)
                ];
            })
        ])->addBody([
            AdminFormElement::columns()
            ->addColumn(function() {
                return [
                    AdminFormElement::html('<h3>Настройки iOS приложения</h3>'),
                    AdminFormElement::text('bundle_id_ios', 'Bundle ID'),
                ];
            })->addColumn(function() {
                return [
                    AdminFormElement::html('<h3>Настройки Android приложения</h3>'),
                    AdminFormElement::text('bundle_id_android', 'Bundle ID'),
                ];
            })
        ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $form = $this->onEdit(null);
        $form->getButtons()->replaceButtons([
            'delete' => null
        ]);

        return  $form;
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
