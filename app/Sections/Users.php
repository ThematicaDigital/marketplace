<?php

namespace App\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Form\FormElements;


use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminSection;
use Meta;
use AdminColumnFilter;

use Illuminate\Support\Facades\Auth;

use App\Models\Companies;
use App\Models\Tags;

/**
 * Class Users
 *
 * @property \Users $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Users extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Пользователи';

    /**
     * @var string
     */
    protected $alias = 'users';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-user')->setPriority(9)->setAccessLogic(function (Page $page) {
            if (Auth::user()->group_id == \App\Models\Users::GROUP_ADMIN) {
                return true;
            }
            return false;
        });
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $tableUsers = AdminDisplay::datatables()
            ->setOrder([[0, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('name', 'Имя'),
                AdminColumn::email('email', 'E-mail'),
                AdminColumn::custom('Группа доступа', function ($instance) {
                    if ($instance->group_id == \App\Models\Users::GROUP_MANAGER) {
                        return "Рекламодатель";
                    } else if ($instance->group_id == \App\Models\Users::GROUP_ADMIN) {
                        return "Администратор";
                    }
                    return "&mdash;";
                })->setOrderable(function($query, $order){
                    return $query->orderBy('group_id', $order);
                }),
                AdminColumn::lists('tags.name', 'Рекламодатели')
            )->paginate(25);

        $tableTags = AdminDisplay::datatables()
            ->setModelClass(Tags::class)
            ->setOrder([[1, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('name', 'Название'),
                AdminColumn::count('companies', 'Кол-во компаний'),
                AdminColumn::count('users', 'Кол-во пользователей')
            )->paginate(25);

        $tabs = AdminDisplay::tabbed();

        $tabs->appendTab(
            $tableUsers,
            'Пользователи'
        );

        $tabs->appendTab(
            $tableTags,
            'Рекламодатели'
        );

        return $tabs;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::select('group_id', 'Группа доступа', [
                \App\Models\Users::GROUP_MANAGER => 'Рекламодатель',
                \App\Models\Users::GROUP_ADMIN => 'Администратор'
            ])->setDefaultValue(\App\Models\Users::GROUP_MANAGER)->required(),
            AdminFormElement::text('name', 'Имя')->required()->addValidationRule('string'),
            AdminFormElement::text('email', 'E-mail')->required()->unique()->addValidationRule('email'),
            AdminFormElement::password('password', 'Пароль')->hashWithBcrypt()->required()->addValidationRule('min:5')->addValidationRule('max:16'),
            AdminFormElement::multiselect('tags', 'Рекламодатели', Tags::class)
                ->setDisplay('name')
                ->taggable()
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $form = $this->onEdit(null);
        $form->getButtons()->replaceButtons([
            'delete' => null
        ]);

        return  $form;
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
