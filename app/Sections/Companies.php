<?php

namespace App\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminSection;
use Meta;

use App\Models\Applications;
use App\Models\Users;
use App\Models\Tags;
use App\Models\Creatives;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class Companies
 *
 * @property \Models/Companies $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Companies extends Section implements Initializable
{
    /**
     * Model
     * @var string
     */
    protected $model = '\App\Models\Companies';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Кампании';

    /**
     * @var string
     */
    protected $alias = 'companies';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-list')->setPriority(11)->setAccessLogic(function (Page $page) {
            if (Auth::user()->group_id == Users::GROUP_ADMIN) {
                return true;
            }
            return false;
        });
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay(Request $request)
    {
        $display = AdminDisplay::datatablesAsync()
            ->setOrder([[0, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('title', 'Название'),
                AdminColumn::datetime('start_date', 'Дата начала')->setFormat('d-m-Y')->setWidth(150),
                AdminColumn::datetime('end_date', 'Дата окончания')->setFormat('d-m-Y')->setWidth(150),
                AdminColumn::custom('Тип соединения', function ($instance) {
                    if ($instance->connection_type == \App\Models\Companies::CONNECTION_WIFI) {
                        return 'Wi-Fi';
                    } elseif ($instance->connection_type == \App\Models\Companies::CONNECTION_MOBILE) {
                        return 'Mobile';
                    } else {
                        return 'Both';
                    }
                })->setWidth(150)->setOrderable(function($query, $order){
                    return $query->orderBy('connection_type', $order);
                }),
                AdminColumn::custom('Платформа', function ($instance) {
                    if ($instance->platform == \App\Models\Applications::PLATFORM_ANDROID) {
                        return 'Android';
                    } elseif ($instance->platform == \App\Models\Applications::PLATFORM_IOS) {
                        return 'iOS';
                    } else {
                        return 'Both';
                    }
                })->setWidth(150)->setOrderable(function($query, $order){
                    return $query->orderBy('platform', $order);
                }),
                AdminColumn::text('limit_views', 'Лимит показов')->setWidth(200),
                AdminColumnEditable::checkbox('active', 'Да', 'Нет')->setLabel('Активация'),
                AdminColumn::lists('tags.name', 'Рекламодатели')
            )->paginate(25);

        $tags = AdminFormElement::select('tags', 'Рекламодатель', Tags::class)
            ->setDisplay('name')
            ->nullable()
            ->setDefaultValue(null);

        $applications = AdminFormElement::select('applications', 'Приложения', Applications::class)
            ->nullable()
            ->setDefaultValue(null);

        $platforms = AdminFormElement::select('platforms', 'Платформа', [
                null => 'Both',
                \App\Models\Applications::PLATFORM_ANDROID => 'Android',
                \App\Models\Applications::PLATFORM_IOS => 'iOS',
            ])
            ->nullable()
            ->setSortable(false)
            ->setDefaultValue(null);

        if (!empty($request->applications)) {
            $display->setApply(function ($query) use ($request) {
                $query->leftJoin('applications_companies_pivot', 'applications_companies_pivot.company_id', '=', 'companies.id')
                    ->where('application_id', $request->applications);
            });
        }

        if (!empty($request->tags)) {
            $display->setApply(function ($query) use ($request) {
                $query->leftJoin('tags_companies_pivot', 'tags_companies_pivot.company_id', '=', 'companies.id')
                    ->where('tag_id', $request->tags);
            });
        }

        if (!empty($request->platforms)) {
            $display->setApply(function ($query) use ($request) {
                $query->where('platform', $request->platforms);
            });
        }

        $display->getActions()->setView(view(
            'admin.campaigns-filters',
            compact('applications', 'tags', 'platforms')
        ))->setPlacement('panel.buttons');

        $control = $display->getColumns()->getControlColumn();

        $button = new \SleepingOwl\Admin\Display\ControlButton(function (\Illuminate\Database\Eloquent\Model $model) {
           return '/admin/companies/' . $model->getKey() . '/copy';
        }, 'Копировать', 50);
        $button->hideText();
        $button->setIcon('fa fa-clone');
        $button->setHtmlAttribute('class', 'btn-success');
        $control->addButton($button)->setWidth('100px');

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        Meta::addJs('admin-custom-companies', asset('packages/sleepingowl/companies.js'), 'admin-default');

        $form = AdminForm::panel();

        $company = \App\Models\Companies::where('id', $id)->first();

        // Select attach for creatives in cmpany
        $creatives = AdminSection::getModel(\App\Models\Creatives::class)->fireDisplay();
        $creatives->getScopes()->push(['withCompany', $id]);
        $creatives->setParameter('companies', $id);
        $creatives->getActions()->setView(view(
            'admin.attach-creatives',
            [
                'creatives' => Creatives::whereNotIn('id', $company->creatives->pluck('id'))->get(),
                'company_id' => $id
            ]
        ))->setPlacement('panel.heading.actions');

        // Button detach for creatives oin company
        $control = $creatives->getColumns()->getControlColumn();
        $button = new \SleepingOwl\Admin\Display\ControlLink(function (\Illuminate\Database\Eloquent\Model $model) use ($id) {
           return '/admin/creatives/' . $model->getKey() . '/' . $id . '/detach';
        }, 'Открепить', 50);
        $button->hideText();
        $button->setIcon('fa fa-times');
        $button->setHtmlAttribute('class', 'btn-warning');
        $control->addButton($button)->setWidth('100px');

        $tabs = AdminDisplay::tabbed([
            'Настройки кампании' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::text('title', 'Название')->required()->addValidationRule('string'),
                AdminFormElement::multiselect('applications', 'Приложения', Applications::class)
                    ->setHelpText('Приложения, где рекламируем')
                    ->required(),
                AdminFormElement::multiselect('tags', 'Рекламодатели', Tags::class)
                    ->setDisplay('name')
                    ->taggable(),
                AdminFormElement::date('start_date', 'Дата начала')->required(),
                AdminFormElement::date('end_date', 'Дата окончания'),
                AdminFormElement::select('platform', 'Платформа', [
                    null => 'Both',
                    \App\Models\Applications::PLATFORM_ANDROID => 'Android',
                    \App\Models\Applications::PLATFORM_IOS => 'iOS',
                ])->setSortable(false),
                AdminFormElement::select('connection_type', 'Тип соединения', [
                    \App\Models\Companies::CONNECTION_WIFI => 'Wi-Fi',
                    \App\Models\Companies::CONNECTION_MOBILE => 'Mobile',
                    \App\Models\Companies::CONNECTION_BOTH => 'Both',
                ])->required(),
                AdminFormElement::select('priority', 'Приоритет')->setEnum(range(1, 5)),
                AdminFormElement::text('limit_views', 'Лимит на показы'),
                AdminFormElement::checkbox('active', 'Активация')
            ]),
            'Креативы кампании' => new \SleepingOwl\Admin\Form\FormElements([
                $creatives
            ]),
        ]);
        $form->addElement($tabs);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $form = AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Название')->required()->addValidationRule('string'),
            AdminFormElement::multiselect('applications', 'Приложения', Applications::class)
                ->setHelpText('Приложения, где рекламируем')
                ->required(),
            AdminFormElement::multiselect('tags', 'Рекламодатели', Tags::class)
                ->setDisplay('name')
                ->taggable(),
            AdminFormElement::date('start_date', 'Дата начала')->setFormat('Y-m-d')->setPickerFormat('Y-m-d')->setCurrentDate()->required(),
            AdminFormElement::date('end_date', 'Дата окончания')->setFormat('Y-m-d')->setPickerFormat('Y-m-d'),
            AdminFormElement::select('platform', 'Платформа', [
                null => 'Both',
                \App\Models\Applications::PLATFORM_ANDROID => 'Android',
                \App\Models\Applications::PLATFORM_IOS => 'iOS',
            ])->setSortable(false),
            AdminFormElement::select('connection_type', 'Тип соединения', [
                \App\Models\Companies::CONNECTION_WIFI => 'Wi-Fi',
                \App\Models\Companies::CONNECTION_MOBILE => 'Mobile',
                \App\Models\Companies::CONNECTION_BOTH => 'Both',
            ])->setDefaultValue(\App\Models\Companies::CONNECTION_BOTH)->required(),
            AdminFormElement::select('priority', 'Приоритет')->setEnum(range(1, 5)),
            AdminFormElement::text('limit_views', 'Лимит на показы'),
            AdminFormElement::checkbox('active', 'Активация'),
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null
        ]);

        return  $form;
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
