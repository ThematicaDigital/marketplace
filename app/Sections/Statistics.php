<?php

namespace App\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;

use AdminDisplayFilter;
use AdminColumnFilter;
use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Meta;

use \App\Models\Applications;
use \App\Models\Creatives;
use \App\Models\Users;
use \App\Models\Companies;

use Illuminate\Support\Facades\Auth;

/**
 * Class Statistics
 *
 * @property \Statistics $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Statistics extends Section implements Initializable
{
    /**
     * Model
     * @var string
     */
    protected $model = '\App\Models\Statistics';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Статистика';

    /**
     * @var string
     */
    protected $alias = 'statistics';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-line-chart')->setPriority(20);
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        Meta::addCss('admin-custom-css-statistics', asset('packages/sleepingowl/statistics.css'));
        Meta::addJs('admin-custom-js-statistics', asset('packages/sleepingowl/statistics.js'), 'admin-default');

        $display = AdminDisplay::datatablesAsync()
            ->setOrder([[1, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::checkbox(),
                AdminColumn::custom('Приложение', function ($instance) {
                    $application = Applications::where('id', $instance->application_id)->first();
                    if ($application) {
                        return "<a href='/admin/applications/{$application->id}/edit'>{$application->title}</a>";
                    }
                    return $instance->application_title;
                })->setWidth('50px')->setOrderable(function($query, $order){
                    return $query->orderBy('application_id', $order);
                }),
                AdminColumn::datetime('date', 'Дата')->setFormat('d-m-Y'),
                AdminColumn::custom('Платформа', function ($instance) {
                    if ($instance->platform == Applications::PLATFORM_ANDROID) {
                        return 'Android';
                    }
                    return 'iOS';
                })->setWidth('50px')->setOrderable(function($s){}),
                AdminColumn::custom('Кампания', function ($instance) {
                    $application = Applications::where('id', $instance->application_id)->first();
                    $company = Companies::where('id', $instance->company_id)->first();
                    $return = '';
                    if ($application) {
                        $return .= "<a href='/admin/applications/{$application->id}/edit'>{$application->title}</a>";
                    } else {
                        $return .= $instance->application_title;
                    }
                    $return .= ' > ';
                    if ($company) {
                        $return .= "<a href='/admin/companies/{$company->id}/edit'>{$company->title}</a>";
                    } else {
                        $return .= $instance->company_title;
                    }
                    return $return;
                })->setOrderable(function($query, $order){
                    return $query->orderBy('application_id', $order);
                }),
                AdminColumn::custom('Креатив', function ($instance) {
                    $creative = Creatives::where('id', $instance->creative_id)->first();
                    if ($creative) {
                        return "<a href='/admin/creatives/{$creative->id}/edit'>{$creative->title}</a>";
                    }
                    return $instance->creative_title;
                })->setOrderable(function($query, $order){
                    return $query->orderBy('creative_id', $order);
                }),
                AdminColumn::custom('Баннер', function ($instance) {
                    return "<a href='javascript:void(0)' class='fa fa-plus show-banners'
                            data-application-id='$instance->application_id'
                            data-creative-id='$instance->creative_id'
                            data-company-id='$instance->company_id'
                            data-date='$instance->date'
                            data-platform='$instance->platform'
                            style='width: 100%; padding-left: 21px;'></a>";
                })->setWidth('104px'),
                AdminColumn::custom('Рекламодатель', function($instance) {
                    $company = Companies::where('id', $instance->company_id)->first();
                    $return = '';
                    if ($company) {
                        foreach ($company->tags as $tag) {
                            $return .= "<span class='label label-info' style='margin-right:2px'>{$tag->name}</span>";
                        }
                    }
                    return $return;
                }),
                AdminColumn::text('conversion_total', 'Переходы')->setWidth('50px'),
                AdminColumn::text('click_total', 'Клики')->setWidth('50px'),
                AdminColumn::custom('CTR', function ($instance) {
                    if ($instance->view_total > 0) {
                        return round((($instance->click_total/$instance->view_total))*100) . "%";
                    }
                    return "&mdash;";
                })->setOrderable(function($s){})->setWidth('50px'),
                AdminColumn::text('view_total', 'Показы')->setWidth('50px'),
                AdminColumn::text('viewed_total', 'Просмотры')->setWidth('50px')
            )->paginate(25);

        if (Auth::user()->group_id == \App\Models\Users::GROUP_MANAGER) {
            $display->setApply(function ($query) {
                $query->leftJoin('users_companies_pivot', 'statistics.application_id', '=', 'users_companies_pivot.company_id');
                $query->where('users_companies_pivot.user_id', Auth::user()->id);
            });
        }

        \DB::statement('SET sql_mode = ""');

        $display->getApply()->push(function ($query) {
            $query->select(\DB::raw('*, SUM(conversion) as conversion_total, SUM(click) as click_total, SUM(view) as view_total, SUM(viewed) as viewed_total'));
            $query->groupBy('application_id', 'creative_id', 'statistics.company_id', 'date', 'platform');
        });
        $display->setDistinct('statistics.id');


        $display->setColumnFilters([
            null,
            AdminColumnFilter::text()->setPlaceholder('Приложение')->setHtmlAttribute('style', 'width:120px'),
            AdminColumnFilter::range()->setFrom(
                AdminColumnFilter::date()->setPlaceholder('От')->setFormat('d-m-Y')->setPickerFormat('d-m-Y')
            )->setTo(
                AdminColumnFilter::date()->setPlaceholder('До')->setFormat('d-m-Y')->setPickerFormat('d-m-Y')
            ),
            AdminColumnFilter::select([
                null => 'Both',
                'Android' => 'Android',
                'iOS' => 'iOS',
            ], 'platform')->setSortable(false)->setPlaceholder('Платформа'),
            AdminColumnFilter::text()->setPlaceholder('Кампания')->setHtmlAttribute('style', 'width:140px'),
            AdminColumnFilter::text()->setPlaceholder('Креатив')->setHtmlAttribute('style', 'width:140px'),
            null,
            AdminColumnFilter::text()->setPlaceholder('Рекламодатель')->setHtmlAttribute('style', 'width:140px'),
            AdminColumnFilter::range()->setFrom(
                AdminColumnFilter::text()->setPlaceholder('От')
            )->setTo(
                AdminColumnFilter::text()->setPlaceholder('До')
            )->setHtmlAttribute('style', 'width:50px'),
            AdminColumnFilter::range()->setFrom(
                AdminColumnFilter::text()->setPlaceholder('От')
            )->setTo(
                AdminColumnFilter::text()->setPlaceholder('До')
            )->setHtmlAttribute('style', 'width:50px'),
            AdminColumnFilter::range()->setFrom(
                AdminColumnFilter::text()->setPlaceholder('От')
            )->setTo(
                AdminColumnFilter::text()->setPlaceholder('До')
            )->setHtmlAttribute('style', 'width:50px'),
            AdminColumnFilter::range()->setFrom(
                AdminColumnFilter::text()->setPlaceholder('От')
            )->setTo(
                AdminColumnFilter::text()->setPlaceholder('До')
            )->setHtmlAttribute('style', 'width:50px'),
            AdminColumnFilter::range()->setFrom(
                AdminColumnFilter::text()->setPlaceholder('От')
            )->setTo(
                AdminColumnFilter::text()->setPlaceholder('До')
            )->setHtmlAttribute('style', 'width:50px')
        ]);

        return $display->setView(view('admin.statistics'));
    }

    public function isDeletable(\Illuminate\Database\Eloquent\Model $model)
    {
        return false;
    }
}