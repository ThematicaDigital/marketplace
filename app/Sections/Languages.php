<?php

namespace App\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use DB;

use \App\Models\Users;

use Illuminate\Support\Facades\Auth;

/**
 * Class Languages
 *
 * @property \Models/Languages $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Languages extends Section implements Initializable
{
    /**
     * Model
     * @var string
     */
    protected $model = '\App\Models\Languages';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Локализация';

    /**
     * @var string
     */
    protected $alias = 'languages';

    /**
     * Init class
     */
    public function initialize()
    {

    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setOrder([[0, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('title', 'Название'),
                AdminColumn::count('banners', 'Кол-во баннеров')
            )->paginate(25);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Название')->required()->addValidationRule('string')->unique()
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $form = $this->onEdit(null);
        $form->getButtons()->replaceButtons([
            'delete' => null
        ]);

        return  $form;
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
