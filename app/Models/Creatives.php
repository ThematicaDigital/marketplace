<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use \App\Models\Companies;

class Creatives extends Model
{
	use SoftDeletes;
	use ValidatingTrait;

    /**
	 * Table name
	 * @var string
	 */
    protected $table = 'creatives';

    /**
     * Fillable fields
     * @var array
     */
    protected $fillable = [
        'title',
        'companies',
    ];

    /**
     * Validation messages
     * @var array
     */
    protected $validationMessages = [
        'required' => 'Поле обязательно для заполнения',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
    ];

    /**
     * Get banners
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function banners()
    {
        return $this->hasMany(\App\Models\Banners::class, 'creative_id');
    }

    /**
     * Get companies
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function companies()
    {
        return $this->belongsToMany(\App\Models\Companies::class, 'companies_creatives_pivot', 'creative_id', 'company_id');
    }

    /**
     * Scope creatives with company
     * @param \Illuminate\Database\Query
     * @param integer
     * @return \Illuminate\Database\Query
     */
    public function scopeWithCompany($query, $company_id)
    {
        $query->leftJoin('companies_creatives_pivot', 'companies_creatives_pivot.creative_id', '=', 'creatives.id')
            ->where('companies_creatives_pivot.company_id', $company_id);
    }

    /**
     * Set companies attribute
     * @param string $companies
     */
    public function setCompaniesAttribute($companies)
    {
        $this->companies()->detach();
        if (!$companies) return;
        if (!$this->exists) $this->save();
        $this->companies()->attach($companies);
    }
}
