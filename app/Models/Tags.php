<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    /**
     * Get users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(\App\Models\Users::class, 'tags_users_pivot', 'tag_id', 'user_id');
    }

    /**
     * Get companies
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function companies()
    {
        return $this->belongsToMany(\App\Models\Companies::class, 'tags_companies_pivot', 'tag_id', 'company_id');
    }
}
