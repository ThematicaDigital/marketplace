<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use \App\Models\Applications;
use \App\Models\Screensizes;

class Banners extends Model
{
	use SoftDeletes;
    use ValidatingTrait;

    /**
     * Banner type image
     * @var integer
     */
    const TYPE_BANNER = 1;

    /**
     * Banner type video
     * @var integer
     */
    const TYPE_VIDEO = 2;

    /**
	 * Table name
	 * @var string
	 */
    protected $table = 'banners';

    /**
     * Fillable fields
     * @var array
     */
    protected $fillable = [
        'title',
        'type',
        'image',
        'language_id',
        'screensize_id',
        'creative_id',
        'link',
    ];

    /**
     * Validation messages
     * @var array
     */
    protected $validationMessages = [
        'required' => 'Поле обязательно для заполнения',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
        'type' => 'required',
        'image' => 'required|string',
        'language_id' => 'required',
        'screensize_id' => 'required',
        'creative_id' => 'required',
    ];

    /**
     * Get screensize
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function screensize()
    {
        return $this->belongsTo(\App\Models\Screensizes::class);
    }

    /**
     * Get language
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo(\App\Models\Languages::class);
    }

    /**
     * Get creative
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function creative()
    {
        return $this->belongsTo(\App\Models\Creatives::class);
    }

    /**
     * Scope banners with creative
     * @param \Illuminate\Database\Query
     * @param integer
     * @return \Illuminate\Database\Query
     */
    public function scopeWithCreative($query, $creative_id)
    {
        return $query->whereHas('creative', function ($q) use ($creative_id) {
            $q->where('creative_id', $creative_id);
        });
    }

    /**
     * Get link for redirect
     *
     * @param Banners $banner
     * @param integer $platform
     *
     * @return string
     */
    public static function getLink(Banners $banner, $platform)
    {
        $medium = date('Ymd', strtotime($banner->creative->company->start_date)) . '_branding_' . $banner->creative_id;
        $source = 'ingame';
        $campaign = 'mobileads';

        if ($banner->creative->company->application->type == Applications::TYPE_WEB) {
            $link = "http://i-mult.ru/?utm_source={$source}&utm_campaign={$campaign}&utm_medium={$medium}&utm_term=redirect";
        } elseif ($banner->creative->company->application->type == Applications::TYPE_MOBILE) {
            if ($platform == Applications::PLATFORM_ANDROID) {
                return "https://play.google.com/store/apps/details?id={$banner->creative->company->application->bundle_id}&referrer=utm_source%3D{$source}%26utm_medium%3D{$medium}%26utm_campaign%3D{$campaign}";
            } elseif ($platform == Applications::PLATFORM_IOS) {
                if (strlen("{$source}_{$campaign}_{$medium}") > 40) {
                    $medium =  'branding_' . $banner->creative_id;
                }
                return "https://itunes.apple.com/app/{$banner->creative->company->application->bundle_id}?pt=118235511&ct={$source}_{$campaign}_{$medium}&mt=8";
            }
        }
        return '';
    }

    /**
     * Boot model
     */
    public static function boot()
    {
        self::saving(function($model) {
            if (!empty($model->screensize_id)) {
                if (is_array($model->screensize_id)) {
                    $screensize = current($model->screensize_id);
                    if (str_contains($screensize, 'x')) {
                        $screensize = preg_replace('/([^0-9x])/', '', $screensize);

                        list($width, $height) = explode('x', $screensize);
                        $orientation = ($width > $height) ? Screensizes::LANDSCAPE : Screensizes::PORTRAIT;

                        $screensize = factory(Screensizes::class)->create([
                            'width' => $width,
                            'height' => $height,
                            'orientation' => $orientation,
                        ]);

                        $model->screensize_id = $screensize->id;
                    } else {
                        $model->screensize_id = $screensize;
                    }
                }
            }
            return $model;
        });

        self::restoring(function($model) {
            $model->deleted_at = null;
            return $model;
        });

        parent::boot();
    }

    /**
     * Calculate aspect ratio of screen
     *
     * @param integer $width Width of screen
     * @param integer $height Height of screen
     *
     * @return string
     */
    public static function ratio($width, $height)
    {
        for ($i = 1; $i <= 40; $i++) {
            $arx = $i * 1.0 * $width/($width + $height);
            $brx = $i * 1.0 * $height/($width + $height);
            if ($i == 40 || (abs($arx - round($arx)) <= 0.02 && abs($brx - round($brx)) <= 0.02)) {
                return round($arx) . ':' . round($brx);
            }
        }
    }
}
