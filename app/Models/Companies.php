<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Companies extends Model
{
	use SoftDeletes;
    use ValidatingTrait;

	/**
	 * Wi-Fi type connection
	 * @var integer
	 */
	const CONNECTION_WIFI = 1;

	/**
	 * Mobile type connection
	 * @var integer
	 */
	const CONNECTION_MOBILE = 2;

	/**
	 * Wi-Fi & mobile type connection
	 * @var integer
	 */
	const CONNECTION_BOTH = 3;

    /**
	 * Table name
	 * @var string
	 */
    protected $table = 'companies';

    /**
     * Fillable fields
     * @var array
     */
    protected $fillable = [
        'title',
        'start_date',
        'end_date',
        'active',
        'connection_type',
        'platform',
        'priority',
        'limit_views',
    ];

    /**
     * Validation messages
     * @var array
     */
    protected $validationMessages = [
        'required' => 'Поле обязательно для заполнения',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
        'start_date' => 'required',
        'connection_type' => 'required',
        'priority' => 'integer|min:1|max:5',
    ];

    /**
     * Get creatives
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function creatives()
    {
        return $this->belongsToMany(\App\Models\Creatives::class, 'companies_creatives_pivot', 'company_id', 'creative_id');
    }

    /**
     * Get applications
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function applications()
    {
        return $this->belongsToMany(\App\Models\Applications::class, 'applications_companies_pivot', 'company_id', 'application_id');
    }

    /**
     * Get users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(\App\Models\Users::class, 'users_companies_pivot', 'company_id', 'user_id');
    }

    /**
     * Get tags
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(\App\Models\Tags::class, 'tags_companies_pivot', 'company_id', 'tag_id');
    }

    /**
     * Encode string connection type to integer
     * @param string $connection_type
     * @return integer
     */
    public static function encodeConnectionType($connection_type)
    {
        switch (strtolower($connection_type)) {
            case 'mobile':
                return self::CONNECTION_MOBILE;
                break;
            case 'wifi':
                return self::CONNECTION_WIFI;
                break;
        }
        return self::CONNECTION_BOTH;
    }

    /**
     * Decode integer connection type to string
     * @param integer $connection_type
     * @return string
     */
    public static function decodeConnectionType($connection_type)
    {
        if ($connection_type == self::CONNECTION_MOBILE) {
            return 'mobile';
        } else if ($connection_type == self::CONNECTION_WIFI) {
            return 'wifi';
        }
        return 'wifi';
    }
}
