<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Applications extends Model
{
	use SoftDeletes;
    use ValidatingTrait;

    /**
     * Platform Android
     * @var integer
     */
    const PLATFORM_ANDROID = 1;

    /**
     * Platform iOS
     * @var integer
     */
    const PLATFORM_IOS = 2;

    /**
	 * Table name
	 * @var string
	 */
    protected $table = 'applications';

    /**
     * Fillable fields
     * @var array
     */
    protected $fillable = [
        'title',
        'bundle_id_ios',
        'bundle_id_android',
        'frequence_banner_time',
        'frequence_banner_session',
        'frequence_video_time',
        'frequence_video_session',
        'frequence_other_time',
        'frequence_other_session',
    ];

    /**
     * Validation messages
     * @var array
     */
    protected $validationMessages = [
        'required' => 'Поле обязательно для заполнения',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
        'frequence_banner_time' => 'required|integer',
        'frequence_banner_session' => 'required|integer',
        'frequence_video_time' => 'required|integer',
        'frequence_video_session' => 'required|integer'
    ];

    /**
     * Get companies
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function companies2()
    {
        return $this->belongsToMany(\App\Models\Companies::class, 'applications_companies_pivot', 'application_id', 'company_id');
    }

    /**
     * Get languages
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function languages()
    {
        return $this->belongsToMany(\App\Models\Languages::class, 'applications_languages_pivot', 'application_id', 'language_id');
    }

    /**
     * Get screensizes
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function screensizes()
    {
        return $this->belongsToMany(\App\Models\Screensizes::class, 'applications_screensizes_pivot', 'application_id', 'screensize_id');
    }

    /**
     * Get companies
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function companies()
    {
        return $this->hasMany(\App\Models\Companies::class, 'application_id');
    }

    /**
     * Encode string platform to integer
     * @param string $platform
     * @return integer|boolean
     */
    public static function encodePlatform($platform)
    {
        if (strtolower($platform) == 'android') {
            return self::PLATFORM_ANDROID;
        } else if (strtolower($platform) == 'ios') {
            return self::PLATFORM_IOS;
        }
        throw new \Exception('Platform not found', 404);
        return false;
    }

    /**
     * Decode integer platform to string
     * @param integer $integer
     * @return string|boolean
     */
    public static function decodePlatform($platform)
    {
        if ($platform == self::PLATFORM_ANDROID) {
            return 'android';
        } else if ($platform == self::PLATFORM_IOS) {
            return 'ios';
        }
        return false;
    }
}
