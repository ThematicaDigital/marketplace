<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Screensizes extends Model
{
	use ValidatingTrait;
	use SoftDeletes;

	/**
	 * Lanscape orientation
	 *
	 * @var integer
	 */
	const LANDSCAPE = 1;

	/**
	 * Portrait orientation
	 *
	 * @var integer
	 */
	const PORTRAIT = 2;

	/**
	 * Table name
	 * @var string
	 */
    protected $table = 'screensizes';

    /**
     * Fillable fields
     * @var array
     */
    protected $fillable = [
        'width',
        'height',
        'orientation'
    ];

    /**
     * Validation messages
     * @var array
     */
    protected $validationMessages = [
        'required' => 'Поле обязательно для заполнения',
        'integer' => 'Поле обязательно для заполнения',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'width' => 'required|integer',
        'height' => 'required|integer',
        'orientation' => 'required|integer',
    ];

    /**
     * Get banners
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function banners()
    {
        return $this->hasMany(\App\Models\Banners::class, 'screensize_id');
    }

    /**
     * Get applications
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function applications()
    {
        return $this->belongsToMany(\App\Models\Applications::class, 'applications_screensizes_pivot', 'screensize_id', 'application_id');
    }

    public function scopeWithCreativeId($query, $creative_id)
    {
        $query->leftJoin('applications_screensizes_pivot', 'applications_screensizes_pivot.screensize_id', '=', 'screensizes.id')
            ->leftJoin('applications_companies_pivot', 'applications_companies_pivot.application_id', '=', 'applications_screensizes_pivot.application_id')
            ->leftJoin('companies_creatives_pivot', 'companies_creatives_pivot.company_id', '=', 'applications_companies_pivot.company_id')
            ->where('companies_creatives_pivot.creative_id', $creative_id);

    }

    public static function boot()
    {
        self::saving(function($model){
            if (!empty($model->title)) {
                if (str_contains($model->title, 'x')) {
                    list($width, $height) = explode('x', $model->title);
                    $orientation = ($width > $height) ? Screensizes::LANDSCAPE : Screensizes::PORTRAIT;
                    $model->width = $width;
                    $model->height = $height;
                    $model->orientation = $orientation;
                    unset($model->title);
                } else {
                    return;
                }
            }
            return $model;
        });
        parent::boot();
    }
}
