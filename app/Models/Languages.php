<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Languages extends Model
{
	use SoftDeletes;
	use ValidatingTrait;

    /**
	 * Table name
	 * @var string
	 */
    protected $table = 'languages';

    /**
     * Fillable fields
     * @var array
     */
    protected $fillable = [
        'title',
    ];

    /**
     * Validation messages
     * @var array
     */
    protected $validationMessages = [
        'required' => 'Поле обязательно для заполнения',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
    ];

    /**
     * Get banners
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function banners()
    {
        return $this->hasMany(\App\Models\Banners::class, 'language_id');
    }

    /**
     * Get applications
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function applications()
    {
        return $this->belongsToMany(\App\Models\Applications::class, 'applications_languages_pivot', 'language_id', 'application_id');
    }

    public function scopeWithCreativeId($query, $creative_id)
    {
        $query->leftJoin('applications_languages_pivot', 'applications_languages_pivot.language_id', '=', 'languages.id')
            ->leftJoin('applications_companies_pivot', 'applications_companies_pivot.application_id', '=', 'applications_languages_pivot.application_id')
            ->leftJoin('companies_creatives_pivot', 'companies_creatives_pivot.company_id', '=', 'applications_companies_pivot.company_id')
            ->where('companies_creatives_pivot.creative_id', $creative_id);
    }
}