<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

use \App\Models\Banners;
use \App\Models\Applications;
use \App\Models\Companies;

class Statistics extends Model
{
	use ValidatingTrait;

	/**
     * Platform Android
     * @var integer
     */
    const PLATFORM_ANDROID = 1;

    /**
     * Platform iOS
     * @var integer
     */
    const PLATFORM_IOS = 2;

    /**
	 * Table name
	 * @var string
	 */
    protected $table = 'statistics';

    /**
     * Fillable fields
     * @var array
     */
    protected $fillable = [
        'application_title',
        'application_id',
        'company_title',
        'company_id',
        'creative_title',
        'creative_id',
        'banner_title',
        'banner_id',
        'date',
        'platform',
        'conversion',
        'click',
        'view',
        'viewed',
    ];

    /**
     * Validation messages
     * @var array
     */
    protected $validationMessages = [
        'required' => 'Поле обязательно для заполнения',
        'integer' => 'Поле обязательно для заполнения',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'application_title' => 'required|string',
        'application_id' => 'required|integer',
        'company_title' => 'required|string',
        'company_id' => 'required|integer',
        'creative_title' => 'required|string',
        'creative_id' => 'required|integer',
        'banner_title' => 'nullable|string',
        'banner_id' => 'nullable|integer',
        'date' => 'required',
        'platform' => 'required|integer',
        'conversion' => 'required|integer',
        'click' => 'required|integer',
        'view' => 'required|integer',
        'viewed' => 'required|integer',
    ];

    /**
     * Get screensize
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(\App\Models\Companies::class);
    }

    /**
     * Write in log when banner was started
     *
     * @param Applications $application
     * @param Banners $banner
     * @param string $platform
     *
     * @return  boolean
     */
    public function setView(Applications $application, Banners $banner, Companies $company, $platform)
    {
        $statistic = $this->isExist($application, $banner, $company, $platform);

        if ($statistic) {

            if (!$statistic->banner_id) {
                $statistic->banner_id = $banner->id;
                $statistic->banner_title = $banner->title;
                $statistic->conversion = $statistic->click = $statistic->view = $statistic->viewed = 0;
            }

            $statistic->view++;

            return $statistic->save();
        }

        $statistic = Statistics::create([
            'application_id' => $application->id,
            'application_title' => $application->title,
            'company_id' => $company->id,
            'company_title' => $company->title,
            'creative_id' => $banner->creative->id,
            'creative_title' => $banner->creative->title,
            'banner_id' => $banner->id,
            'banner_title' => $banner->title,
            'date' => date('Y-m-d'),
            'platform' => Applications::encodePlatform($platform),
            'conversion' => 0,
            'click' => 0,
            'view' => 1,
            'viewed' => 0,
        ]);

        return boolval($statistic);
    }

    /**
     * Write in log when banner was viewed before the end
     *
     * @param Applications $application
     * @param Banners $banner
     * @param string $platform
     *
     * @return  boolean
     */
    public function setViewed(Applications $application, Banners $banner, Companies $company, $platform)
    {
        $statistic = $this->isExist($application, $banner, $company, $platform);

        if ($statistic) {

            if (!$statistic->banner_id) {
                $statistic->banner_id = $banner->id;
                $statistic->banner_title = $banner->title;
                $statistic->conversion = $statistic->click = $statistic->viewed = 0;
                $statistic->view = 1;
            }

            $statistic->viewed++;

            return $statistic->save();
        }

        $statistic = Statistics::create([
            'application_id' => $application->id,
            'application_title' => $application->title,
            'company_id' => $company->id,
            'company_title' => $company->title,
            'creative_id' => $banner->creative->id,
            'creative_title' => $banner->creative->title,
            'banner_id' => $banner->id,
            'banner_title' => $banner->title,
            'date' => date('Y-m-d'),
            'platform' => Applications::encodePlatform($platform),
            'conversion' => 0,
            'click' => 0,
            'view' => 1,
            'viewed' => 1,
        ]);

        return boolval($statistic);
    }

    /**
     * Write in log when user clicked on a banner
     *
     * @param Applications $application
     * @param Banners $banner
     * @param string $platform
     *
     * @return  boolean
     */
    public function setClick(Applications $application, Banners $banner, Companies $company, $platform)
    {
        $statistic = $this->isExist($application, $banner, $company, $platform);

        if ($statistic) {

            if (!$statistic->banner_id) {
                $statistic->banner_id = $banner->id;
                $statistic->banner_title = $banner->title;
                $statistic->conversion = $statistic->click = $statistic->viewed = 0;
                $statistic->view = 1;
            }

            $statistic->click++;

            return $statistic->save();
        }

        $statistic = Statistics::create([
            'application_id' => $application->id,
            'application_title' => $application->title,
            'company_id' => $company->id,
            'company_title' => $company->title,
            'creative_id' => $banner->creative->id,
            'creative_title' => $banner->creative->title,
            'banner_id' => $banner->id,
            'banner_title' => $banner->title,
            'date' => date('Y-m-d'),
            'platform' => Applications::encodePlatform($platform),
            'conversion' => 0,
            'click' => 1,
            'view' => 1,
            'viewed' => 0,
        ]);

        return boolval($statistic);
    }

    /**
     * Write in log when user was redirected to banner's link
     *
     * @param Applications $application
     * @param Banners $banner
     * @param string $platform
     *
     * @return  boolean
     */
    public function setRedirect(Applications $application, Banners $banner, Companies $company, $platform)
    {
        $statistic = $this->isExist($application, $banner, $company, $platform);

        if ($statistic) {

            if (!$statistic->banner_id) {
                $statistic->banner_id = $banner->id;
                $statistic->banner_title = $banner->title;
                $statistic->conversion = $statistic->viewed = 0;
                $statistic->click = $statistic->view = 1;
            }

            $statistic->conversion++;

            return $statistic->save();
        }

        $statistic = Statistics::create([
            'application_id' => $application->id,
            'application_title' => $application->title,
            'company_id' => $company->id,
            'company_title' => $company->title,
            'creative_id' => $banner->creative->id,
            'creative_title' => $banner->creative->title,
            'banner_id' => $banner->id,
            'banner_title' => $banner->title,
            'date' => date('Y-m-d'),
            'platform' => Applications::encodePlatform($platform),
            'conversion' => 1,
            'click' => 1,
            'view' => 1,
            'viewed' => 0,
        ]);

        return boolval($statistic);
    }

    /**
     * Find statistic is exist by params
     *
     * @param Applications $application
     * @param Banners $banner
     * @param string $platform
     *
     * @return Statistics|boolean
     */
    public function isExist(Applications $application, Banners $banner, Companies $company, $platform)
    {
        return Statistics::where('application_id', $application->id)
            ->where('creative_id', $banner->creative->id)
            ->where('company_id', $company->id)
            ->where('date', date('Y-m-d'))
            ->where('platform', Applications::encodePlatform($platform))
            ->where(function ($query) use ($banner) {
                $query->whereNull('banner_id')->orWhere('banner_id', $banner->id);
            })
            ->first();
    }
}
