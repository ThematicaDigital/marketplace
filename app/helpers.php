<?php
/**
 * CDN link helper
 * @param string $asset
 * @return string
 */
function cdn($asset)
{
    if(!Config::get('app.cdn'))
        return asset($asset);

    $cdns = Config::get('app.cdn');
    $assetName = basename($asset);

    $assetName = explode("?", $assetName);
    $assetName = $assetName[0];

    if (is_array($cdns)) {
        foreach($cdns as $cdn => $types) {
            if(preg_match('/^.*\.(' . $types . ')$/i', $assetName))
                return cdnPath($cdn, $asset);
        }
        end($cdns);
        return cdnPath(key($cdns), $asset);
    } else {
        return cdnPath($cdns, $asset);
    }
}

/**
 * CDN build path
 * @param string $cdn
 * @param string $asset
 * @return string
 */
function cdnPath($cdn, $asset)
{
    return  "//" . rtrim($cdn, "/") . "/" . ltrim($asset, "/");
}