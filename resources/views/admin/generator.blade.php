<form method="POST" action="" class="panel panel-default" id="generator">
    <div class="form-elements">
        <table style="width:100%">
            <thead>
                <tr>
                    <th style="width:16%">{{ Form::label('game_name', 'Game name') }}</th>
                    <th style="width:16%">{{ Form::label('source', 'Source') }}</th>
                    <th style="width:16%">{{ Form::label('campaign', 'Campaign') }}</th>
                    <th style="width:16%">{{ Form::label('m_date', 'Medium Date') }}</th>
                    <th style="width:16%">{{ Form::label('m_placement', 'Medium Placement') }}</th>
                    <th style="width:16%">{{ Form::label('m_creative', 'Medium Creative') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <select class="form-control input-select" id="game_name" name="game_name">
                            @foreach ($applications as $application)
                                <option value="{{ $application->id }}" data-android="{{ $application->bundle_id_android }}" data-ios="{{ $application->bundle_id_ios }}" data-web="{{ strtolower($application->title) }}">
                                    {{ $application->title }}
                                </option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        {{ Form::select(
                            'source', [
                                'ingame' => 'ingame',
                                'appstore' => 'appstore',
                                'gplay' => 'gplay',
                                'imult' => 'imult',
                                'youtube' => 'youtube',
                                'vk' => 'vk',
                                'ok' => 'ok',
                                'fb' => 'fb',
                                'tlumbar' => 'tlumbar',
                                'smi2' => 'smi2',
                                'imult_en' => 'imult_en'
                            ],
                            null,
                            ['class' => 'form-control input-select']
                        ) }}
                    </td>
                    <td>
                        {{ Form::select(
                            'campaign',
                            [
                                'crossp' => 'crossp',
                                'webads' => 'webads',
                                'mobileads' => 'mobileads',
                                'pr' => 'pr',
                                'descr' => 'descr',
                                'gamepage' => 'gamepage',
                                'main' => 'main',
                                'social' => 'social',
                                'settings' => 'settings',
                                'aso' => 'aso',
                                'showcase' => 'showcase',
                                'news' => 'news',
                                'mobileads_amp' => 'mobileads_amp',
                                'videoads' => 'videoads',
                                'social_ads' => 'social_ads',
                            ],
                            null,
                            ['class' => 'form-control input-select']
                        ) }}
                    </td>
                    <td>
                        <div class="form-group form-element-date">
                            <div class="input-date input-group" style="width:100%;margin-top:14px">
                                {{ Form::text(
                                    'm_date',
                                    null,
                                    [
                                        'class' => 'form-control',
                                        'style' => 'width:',
                                        'data-date-format' => 'DDMMYYYY',
                                        'data-date-pickdate' => 'true',
                                        'data-date-picktime' => 'false',
                                        'data-date-useseconds' => 'false'
                                    ]
                                ) }}
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            </div>
                        </div>
                    </td>
                    <td>
                        {{ Form::select(
                            'm_placement', $screensizes,
                            null,
                            ['class' => 'form-control input-select']
                        ) }}
                    </td>
                    <td>{{ Form::text('m_creative', null, ['class' => 'form-control']) }}</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td>Web:</td>
                    <td colspan="5">
                        <div class="input-group">
                            {{ Form::text('link_web', null, ['class' => 'form-control']) }}
                            <span class="input-group-addon copy-to-clipboard" title="Copy to clipboard">
                                <i class="fa fa-clone" aria-hidden="true"></i>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Google Play:</td>
                    <td colspan="5">
                        <div class="input-group">
                            {{ Form::text('link_android', null, ['class' => 'form-control']) }}
                            <span class="input-group-addon copy-to-clipboard" title="Copy to clipboard">
                                <i class="fa fa-clone" aria-hidden="true"></i>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>AppStore:</td>
                    <td colspan="5">
                        <div class="input-group">
                            {{ Form::text('link_ios', null, ['class' => 'form-control']) }}
                            <span class="input-group-addon copy-to-clipboard" title="Copy to clipboard">
                                <i class="fa fa-clone" aria-hidden="true"></i>
                            </span>
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</form>