@if ( ! empty($title))
	<div class="row">
		<div class="col-lg-12">
			{!! $title !!}
		</div>
	</div>
	<br />
@endif

@yield('before.panel')

<div class="panel panel-default">
	<div class="panel-heading">
		@if ($creatable)
			<a href="{{ url($createUrl) }}" class="btn btn-primary">
				<i class="fa fa-plus"></i> {{ $newEntryButtonText }}
			</a>
		@endif

		@yield('panel.buttons')

		<div class="view-columns pull-left">
			<div>Показывать:</div>
			<div>
				<label><input type="checkbox" data-column="1">Приложение</label>
				<label><input type="checkbox" data-column="2">Дата</label>
				<label><input type="checkbox" data-column="3">Платформа</label>
				<label><input type="checkbox" data-column="4">Кампания</label>
				<label><input type="checkbox" data-column="5">Креатив</label>
				<label><input type="checkbox" data-column="6">Баннер</label>
				<label><input type="checkbox" data-column="7">Рекламодатель</label>
				<label><input type="checkbox" data-column="all">Нет</label>
			</div>
		</div>

		<div class="pull-right">
			<form action="/admin/statistics/export" method="POST" target="_blank" id="export-csv">
				{{ csrf_field() }}
				<button class="btn btn-primary" type="submit">Экспорт в CSV</button>
			</form>
			@yield('panel.heading.actions')
		</div>
		<div style="clear:both"></div>
	</div>

	@yield('panel.heading')

	@foreach($extensions as $ext)
		{!! $ext->render() !!}
	@endforeach

	@yield('panel.footer')
</div>

@yield('after.panel')
