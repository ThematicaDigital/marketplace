<div class="pull-right">
	<form method="GET" action="">
		{{ csrf_field() }}
		<div class="pull-right" style="margin-left:10px">
			<div class="form-group">
				<label class="control-label" style="width:100%">&nbsp;</label>
				<button class="btn btn-primary">
					<i class="fa fa-filter"></i> Фильтровать
				</button>
	    	</div>
		</div>
		<div class="pull-right" style="margin-left:10px;width:300px">
			{!! $applications !!}
		</div>
		<div class="pull-right" style="margin-left:10px;width:300px">
			{!! $tags !!}
		</div>
		<div class="pull-right" style="margin-left:10px;width:300px">
			{!! $platforms !!}
		</div>
		<div class="clearfix"></div>
	</form>
</div>
<div class="clearfix"></div>