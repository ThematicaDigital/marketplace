<div id="attach-creatives">
	<select class="form-control input-select" id="creatives" name="creatives" style="width:300px">
	    @foreach ($creatives as $creative)
	        <option value="{{ $creative->id }}">
	            {{ $creative->title }}
	        </option>
	    @endforeach
	</select>
	<input type="hidden" name="company_id" value="{{ $company_id }}" />
	<button type="button" class="btn btn-primary">
		<i class="fa fa-paperclip"></i> Прикрепить
	</button>
</div>