<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tags')) {
            Schema::create('tags', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('tags_companies_pivot')) {
            Schema::create('tags_companies_pivot', function (Blueprint $table) {
                $table->integer('tag_id')->unsigned();
                $table->integer('company_id')->unsigned();
            });
        }

        if (!Schema::hasTable('tags_users_pivot')) {
            Schema::create('tags_users_pivot', function (Blueprint $table) {
                $table->integer('tag_id')->unsigned();
                $table->integer('user_id')->unsigned();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
        Schema::dropIfExists('tags_companies_pivot');
        Schema::dropIfExists('tags_users_pivot');
    }
}
