<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApplicationsScreensizesPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('applications_screensizes_pivot')) {
            Schema::create('applications_screensizes_pivot', function (Blueprint $table) {
                $table->integer('application_id')->unsigned();
                $table->integer('screensize_id')->unsigned();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications_screensizes_pivot');
    }
}
