<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUniqueForScreensizes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('screensizes', function (Blueprint $table) {
            $table->dropUnique('screensizes_width_height_orientation_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('screensizes', function (Blueprint $table) {
            $table->unique(['width', 'height', 'orientation']);
        });
    }
}
