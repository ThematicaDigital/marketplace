<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScreensizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('screensizes')) {
            Schema::create('screensizes', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('width');
                $table->integer('height');
                $table->tinyInteger('orientation');
                $table->timestamps();
                $table->softDeletes();
                $table->unique(['width', 'height', 'orientation']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('screensizes');
    }
}
