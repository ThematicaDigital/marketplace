<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatisticTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('statistics')) {
            Schema::create('statistics', function (Blueprint $table) {
                $table->increments('id');
                $table->string('application_title');
                $table->integer('application_id');
                $table->string('company_title');
                $table->integer('company_id');
                $table->string('creative_title');
                $table->integer('creative_id');
                $table->date('date');
                $table->tinyInteger('platform');
                $table->integer('conversion')->default(0);
                $table->integer('click')->default(0);
                $table->integer('view')->default(0);
                $table->integer('viewed')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistics');
    }
}
