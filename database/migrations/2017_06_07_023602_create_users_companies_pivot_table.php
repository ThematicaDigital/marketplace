<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Applications;

class CreateUsersCompaniesPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('users_companies_pivot')) {
            Schema::create('users_companies_pivot', function (Blueprint $table) {
                $table->integer('user_id')->unsigned();
                $table->integer('company_id')->unsigned();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('users_companies_pivot')) {
            Schema::dropIfExists('users_companies_pivot');
        }
    }
}
