<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Models\Banners;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('banners')) {
            Schema::create('banners', function (Blueprint $table) {
                $table->increments('id');
                $table->tinyInteger('type')->default(Banners::TYPE_BANNER);
                $table->string('image')->nullable()->default(null);
                $table->string('video')->nullable()->default(null);
                $table->string('link')->nullable()->default(null);

                $table->integer('language_id')->unsigned();

                $table->integer('screensize_id')->unsigned();

                $table->integer('creative_id')->unsigned();

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
