<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBannerFieldsForStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statistics', function (Blueprint $table) {
            $table->string('banner_title')->default('')->after('creative_id');
            $table->integer('banner_id')->nullable()->after('banner_title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('statistics', function (Blueprint $table) {
            $table->dropColumn('banner_title');
        });

        Schema::table('statistics', function (Blueprint $table) {
            $table->dropColumn('banner_id');
        });
    }
}
