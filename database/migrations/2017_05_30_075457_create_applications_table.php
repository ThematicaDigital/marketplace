<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('applications')) {
            Schema::create('applications', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->tinyInteger('type');
                $table->string('bundle_id_ios')->nullable()->default(null);
                $table->string('bundle_id_android')->nullable()->default(null);
                $table->integer('frequence_banner_time')->default(0);
                $table->string('link')->nullable()->default(null);
                $table->integer('frequence_banner_session')->default(0);
                $table->integer('frequence_video_time')->default(0);
                $table->integer('frequence_video_session')->default(0);
                $table->integer('frequence_other_time')->default(0);
                $table->integer('frequence_other_session')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
