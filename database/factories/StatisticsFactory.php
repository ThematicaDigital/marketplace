<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Models\Statistics;
use App\Models\Applications;
use App\Models\Creatives;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Statistics::class, function (Faker\Generator $faker) {

    $platform = $faker->randomElement([
        Applications::PLATFORM_IOS,
        Applications::PLATFORM_ANDROID,
    ]);

    $creative = Creatives::inRandomOrder()->first();

    return [
        'application_id' => $creative->companies()->first()->id,
        'application_title' => $creative->companies()->first()->title,
        'company_id' => $creative->companies()->first()->applications()->first()->id,
        'company_title' => $creative->companies()->first()->applications()->first()->title,
        'creative_id' => $creative->id,
        'creative_title' => $creative->title,
        'date' => $faker->date('Y-m-d'),
        'platform' => $platform,
        'conversion' => $faker->numberBetween(1, 100),
        'click' => $faker->numberBetween(1, 500),
        'view' => $faker->numberBetween(500, 1000),
        'viewed' => $faker->numberBetween(400, 500),
    ];
});
