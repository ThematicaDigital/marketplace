<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Models\Banners;
use App\Models\Languages;
use App\Models\Creatives;
use App\Models\Screensizes;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Banners::class, function (Faker\Generator $faker) {

    $storage = public_path(config('sleeping_owl.filesUploadDirectory'));

    $type = $faker->randomElement([
        Banners::TYPE_BANNER,
        Banners::TYPE_VIDEO,
    ]);

    $screensize = Screensizes::inRandomOrder()->first();

    $image = file_get_contents("http://via.placeholder.com/{$screensize->width}x{$screensize->height}");
    $file = $storage . '/' . hash('md5', $image, false) . '.png';

    file_put_contents($file, $image);

    return [
        'type' => $type,
        'image' => str_replace(public_path() . '/', '', $file),
        'link' => $faker->url(),
        'video' => $type == Banners::TYPE_VIDEO ? 'movie.avi' : null,
        'creative_id' => Creatives::inRandomOrder()->first()->id,
        'language_id' => Languages::inRandomOrder()->first()->id,
        'screensize_id' => $screensize->id,
    ];
});
