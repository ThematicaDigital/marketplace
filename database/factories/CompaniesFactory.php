<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
use App\Models\Companies;
use App\Models\Applications;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Companies::class, function (Faker\Generator $faker) {

    $connection = $faker->randomElement([
        Companies::CONNECTION_WIFI,
        Companies::CONNECTION_MOBILE,
        Companies::CONNECTION_BOTH,
    ]);

    $platform = $faker->randomElement([
        Applications::PLATFORM_ANDROID,
        Applications::PLATFORM_IOS,
    ]);

    return [
        'title' => $faker->word(),
        'start_date' => $faker->date('Y-m-d'),
        'end_date' => $faker->date('Y-m-d', '+ 60 days'),
        'active' => $faker->boolean(),
        'connection_type' => $connection,
        'platform' => $platform,
        'limit_views' => $faker->numberBetween(1, 30),
        'priority' => $faker->numberBetween(1, 5),
    ];
});
