<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Models\Applications;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Applications::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word(),
        'frequence_banner_time' => $faker->numberBetween(1, 99),
        'frequence_banner_session' => $faker->numberBetween(1, 99),
        'frequence_video_time' => $faker->numberBetween(1, 99),
        'frequence_video_session' => $faker->numberBetween(1, 99),
        'frequence_other_time' => $faker->numberBetween(1, 99),
        'frequence_other_session' => $faker->numberBetween(1, 99),
        'bundle_id_ios' => $faker->word(),
        'bundle_id_android' => $faker->word(),
    ];
});
