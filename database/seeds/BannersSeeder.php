<?php

use Illuminate\Database\Seeder;
use App\Models\Banners;

class BannersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Banners::truncate();

        $storage = public_path(config('sleeping_owl.filesUploadDirectory'));
        File::delete(File::allFiles($storage));

        factory(Banners::class, 20)->create();
    }
}
