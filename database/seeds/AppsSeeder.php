<?php

use Illuminate\Database\Seeder;
use App\Models\Applications;
use App\Models\Companies;

class AppsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('applications_companies_pivot')->truncate();
        $applications = Applications::all();
        foreach ($applications as $application) {
            $application->companies()->attach(Companies::inRandomOrder()->first()->id);
        }
    }
}
