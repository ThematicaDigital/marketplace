<?php

use Illuminate\Database\Seeder;
use App\Models\Creatives;
use App\Models\Companies;

class CreativesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Creatives::truncate();

        DB::table('companies_creatives_pivot')->truncate();

        $creatives = factory(Creatives::class, 20)->create();

        $creatives->each(function($creative) {
            $creative->companies()->attach(Companies::inRandomOrder()->limit(5)->get());
        });
    }
}
