<?php

use Illuminate\Database\Seeder;
use App\Models\Screensizes;
use App\Models\Banners;

class ScreensizesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Screensizes::truncate();
        factory(Screensizes::class, 20)->create();
    }
}
