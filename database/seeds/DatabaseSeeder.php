<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TagsSeeder::class);
        $this->call(ScreensizesSeeder::class);
        $this->call(LanguagesSeeder::class);
        $this->call(ApplicationsSeeder::class);
        $this->call(CompaniesSeeder::class);
        $this->call(CreativesSeeder::class);
        $this->call(BannersSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(StatisticsSeeder::class);
    }
}
