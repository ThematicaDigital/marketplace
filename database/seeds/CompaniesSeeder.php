<?php

use Illuminate\Database\Seeder;
use App\Models\Companies;
use App\Models\Tags;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Companies::truncate();

        DB::table('tags_companies_pivot')->truncate();

        $companies = factory(Companies::class, 20)->create();

        $companies->each(function($company) {
            $company->tags()->attach(Tags::inRandomOrder()->limit(5)->get());
        });
    }
}
