<?php

use Illuminate\Database\Seeder;
use App\Models\Applications;
use App\Models\Screensizes;
use App\Models\Languages;
use App\Models\Companies;

class ApplicationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Applications::truncate();
        DB::table('applications_screensizes_pivot')->truncate();
        DB::table('applications_languages_pivot')->truncate();
        DB::table('applications_companies_pivot')->truncate();

        $applications = factory(Applications::class, 20)->create();
        $applications->each(function($application) {
            $application->screensizes()->attach(Screensizes::inRandomOrder()->limit(5)->get());
        });
        $applications->each(function($application) {
            $application->languages()->attach(Languages::inRandomOrder()->limit(5)->get());
        });
        $applications->each(function($application) {
            $application->companies2()->attach(Companies::inRandomOrder()->limit(5)->get());
        });
    }
}
