<?php

use Illuminate\Database\Seeder;
use App\Models\Users;
use App\Models\Companies;
use App\Models\Tags;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Users::truncate();
        DB::table('users_companies_pivot')->truncate();
        DB::table('tags_users_pivot')->truncate();

        Users::create([
            'name' => 'admin',
            'group_id' => Users::GROUP_ADMIN,
            'email' => 'admin@site.com',
            'password' => bcrypt('641146')
        ]);

        $manager = Users::create([
            'name' => 'manager',
            'group_id' => Users::GROUP_MANAGER,
            'email' => 'manager@site.com',
            'password' => bcrypt('641146')
        ]);

        $manager->tags()->attach(Tags::inRandomOrder()->limit(3)->get());
        $manager->companies()->attach(Companies::inRandomOrder()->limit(3)->get());
    }
}
