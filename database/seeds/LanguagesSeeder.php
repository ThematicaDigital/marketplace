<?php

use Illuminate\Database\Seeder;
use App\Models\Languages;

class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Languages::truncate();
        factory(Languages::class, 20)->create();
    }
}
