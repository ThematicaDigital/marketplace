function attachCreatives() {
	$.ajax({
		type: 'POST',
      	url: '/admin/companies/attach-creatives',
      	dataType: 'json',
      	data: $('#attach-creatives select, #attach-creatives input').serialize(),
	  	success: function(response) {
	  		if (response.status == true) {
	  			location.reload();
	  		}
	  	}
  	});
}
$(document).on('click', '#attach-creatives button', function(event) {
	attachCreatives();
});