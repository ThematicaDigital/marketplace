function linkGenerator()
{
	let linkWebElement = $('input[name="link_web"]');
	let linkAndroidElement = $('input[name="link_android"]');
	let linkIosElement = $('input[name="link_ios"]');

	let linkWebValue, linkAndroidValue, linkIosValue, valMedium = '';

	let valGameName = $('[name="game_name"] option:selected');
	let valSource = $('[name="source"] option:selected').val();
	let valCampaign = $('[name="campaign"] option:selected').val();
	let valMDate = $('[name="m_date"]').val();
	let valMPlacement = $('[name="m_placement"] option:selected').val();
	let valMCreative = $('[name="m_creative"]').val();

	valMedium = valMDate + '_' + valMPlacement + '_' + valMCreative;

	linkWebValue = 	'http://i-mult.ru/games/' + valGameName.data('web') +
					'/?utm_source=' + valSource +
					'&utm_campaign=' + valCampaign +
					'&utm_medium=' + valMedium +
					'&utm_term=redirect';

	linkAndroidValue = 	'http://play.google.com/store/apps/details?id=' + valGameName.data('android') +
						'&referrer=utm_source%3D' + valSource +
						'%26utm_medium%3D' + valMedium +
						'%26utm_campaign%3D' + valCampaign;

	linkIosValue = 	'http://itunes.apple.com/app/' + valGameName.data('ios') +
					'?pt=118235511&ct=' + valSource +
					'_' + valCampaign +
					'_' + valMedium + '&mt=8';

	linkWebElement.val(linkWebValue);
	linkAndroidElement.val(linkAndroidValue);
	linkIosElement.val(linkIosValue);
}

$(document).on('change paste keyup', '#generator input[type="text"]', function(event) {
	linkGenerator();
});

$(document).on('change', '#generator select', function(event) {
	linkGenerator();
});

$(".input-date").on("dp.change", function (e) {
    linkGenerator();
});

$(document).on('click', '.copy-to-clipboard', function(event) {
	let element = $(this).parents('.input-group').find('input');
	element.select();
	document.execCommand("copy");
});

linkGenerator();
$('.input-date').data("DateTimePicker").date('20072018');