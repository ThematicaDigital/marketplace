$('.view-columns input').on('change', function (e) {
    e.preventDefault();
    var index = $(this).attr('data-column');
    if (this.checked) {
    	if (index == 'all') {
    		$('.view-columns input').prop('checked', true);
    		$('.datatables').dataTable().api().column(1).visible(false);
    		$('.datatables').dataTable().api().column(2).visible(false);
    		$('.datatables').dataTable().api().column(3).visible(false);
    		$('.datatables').dataTable().api().column(4).visible(false);
            $('.datatables').dataTable().api().column(5).visible(false);
    		$('.datatables').dataTable().api().column(6).visible(false);
    	} else {
    		$('.datatables').dataTable().api().column(index).visible(false);
    	}
    } else {
    	if (index == 'all') {
    		$('.view-columns input').prop('checked', false);
    		$('.datatables').dataTable().api().column(1).visible(true);
    		$('.datatables').dataTable().api().column(2).visible(true);
    		$('.datatables').dataTable().api().column(3).visible(true);
    		$('.datatables').dataTable().api().column(4).visible(true);
            $('.datatables').dataTable().api().column(5).visible(true);
    		$('.datatables').dataTable().api().column(6).visible(true);
    	} else {
    		$('.datatables').dataTable().api().column(index).visible(true);
    	}
    }
});

$('#export-csv').on('submit', function (e) {

    $('#export-csv input[type="hidden"][name="ids[]"]').remove();
	$('.datatables input[type="checkbox"]:checked').each(function(index) {
        $('#export-csv').append('<input type="hidden" name="ids[]" value="' + $(this).val() + '" />');
    });

	var filterMap = {
		"application_title": $('.display-filters td[data-index="1"] input[type="text"]').val(),
		"date_from":  $('.display-filters td[data-index="2"] input[type="text"]').eq(0).val(),
		"date_to": $('.display-filters td[data-index="2"] input[type="text"]').eq(1).val(),
		"platform": $('.display-filters td[data-index="3"] .select2-selection__rendered').html(),
		"company_title": $('.display-filters td[data-index="4"] input[type="text"]').val(),
		"creative_title": $('.display-filters td[data-index="5"] input[type="text"]').val(),
		"tag_name": $('.display-filters td[data-index="7"] input[type="text"]').val(),
		"conversion_total_from": $('.display-filters td[data-index="8"] input[type="text"]').eq(0).val(),
		"conversion_total_to": $('.display-filters td[data-index="8"] input[type="text"]').eq(1).val(),
		"click_total_from": $('.display-filters td[data-index="9"] input[type="text"]').eq(0).val(),
		"click_total_to": $('.display-filters td[data-index="9"] input[type="text"]').eq(1).val(),
		"ctr_from": $('.display-filters td[data-index="10"] input[type="text"]').eq(0).val(),
		"ctr_to": $('.display-filters td[data-index="10"] input[type="text"]').eq(1).val(),
		"view_total_from": $('.display-filters td[data-index="11"] input[type="text"]').eq(0).val(),
		"view_total_to": $('.display-filters td[data-index="11"] input[type="text"]').eq(1).val(),
		"viewed_total_from": $('.display-filters td[data-index="12"] input[type="text"]').eq(0).val(),
		"viewed_total_to": $('.display-filters td[data-index="12"] input[type="text"]').eq(1).val()
	};

	for (var key in filterMap) {
		$('#export-csv input[type="hidden"][name="' + key +'"]').remove();
		if (filterMap[key]) {
			$('#export-csv').append('<input type="hidden" name="' + key + '" value="' + filterMap[key] + '" />');
		}
	}
});

$('thead').clone().addClass('stub').prependTo($('thead').parent());

$(window).bind('scroll', function() {
	if ($(this).scrollTop() >= $('table').offset().top) {
		$('thead:not(.stub)').addClass('fixed');
		$('thead.stub').show();
	} else {
		$('thead:not(.stub)').removeClass('fixed');
		$('thead.stub').hide();
	}
});

$(".table thead, .dt-bootstrap-header").bind("DOMSubtreeModified", fetchPerBannerStatistics);
$("input, select").change(fetchPerBannerStatistics);

$('.show-banners').click(function (e) {
	var id = $(e.target).parents('tr').find("input[name='_id[]']").val();
	if ($(e.target).hasClass('fa-plus') && $('tr[data-reference-id="' + id + '"]').length > 0) {
		$('tr[data-reference-id="' + id + '"]').fadeIn();
		$(e.target).removeClass('fa-plus');
		$(e.target).addClass('fa-minus');
	} else if ($(e.target).hasClass('fa-minus')) {
		$('tr[data-reference-id="' + id + '"]').fadeOut();
		$(e.target).removeClass('fa-minus');
		$(e.target).addClass('fa-plus');
	}
});

function fetchPerBannerStatistics() {

	var ids = [];

	$("input[name='_id[]']").each(function (i, e) {
		ids.push($(e).val());
	});

	if ($("tr.clone").length == 0) {
		$.ajax({
			url: '/admin/statistics/banner',
			data: { ids: ids }
		}).done(function(data) {
			if ($("tr.clone").length == 0) {
				$('.show-banners.fa-minus').removeClass('fa-minus').addClass('fa-plus');
				data.forEach(function(item) {
					var row = $("a[data-application-id='" + item.application_id + "'][data-creative-id='" + item.creative_id + "'][data-company-id='" + item.company_id + "'][data-date='" + item.date + "'][data-platform='" + item.platform + "']").parents('tr');
					var clone = row.clone();
					clone.attr('data-reference-id', row.find("input[name='_id[]']").val()).hide();
					clone.addClass('clone');
					clone.find("td").eq(0).text('');
					clone.find("td").eq(1).text('');
					clone.find("td").eq(2).text('');
					clone.find("td").eq(3).text('');
					clone.find("td").eq(4).text('');
					clone.find("td").eq(5).text('');
					clone.find("td").eq(6).html('<a target="_blank" href="/admin/banners/' + item.banner_id + '/edit">' + (item.banner_title ? item.banner_title : 'No name') +'</a>');
					clone.find("td").eq(7).text('');
					clone.find("td").eq(8).text(item.conversion);
					clone.find("td").eq(9).text(item.click);
					clone.find("td").eq(10).text(Math.round((item.click / item.view) * 100) + '%');
					clone.find("td").eq(11).text(item.view);
					clone.find("td").eq(12).text(item.viewed);
					row.after(clone);
					row.find('a.show-banners').show();
				});
			}
		});
	}
}

fetchPerBannerStatistics();

