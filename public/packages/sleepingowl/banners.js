function bannersTypeChange() {
	if ($('#type').val() == '1') {
		$('input[name="video"]').parents('.form-group').hide();
		$('label[for="image"]').text('Изображение');
	} else {
		$('input[name="video"]').parents('.form-group').show();
		$('label[for="image"]').text('Превью видео');
	}
}

function bannersInitImage() {
	$('.form-element-files__item').each(function(index, item) {
		var url = $(item).find('.form-element-files__info .btn').attr('href');
		$(item).find('.form-element-files__info .btn').attr('download', '');
		$(item).find('.form-element-files__file').css({
			'background': 'url(' + url + ')',
			'background-size': 'cover',
			'min-height': '140px'
		});
		$(item).find('.form-element-files__file i').hide();
	});
}

function bannersCreativeIdChange() {
	var creative_id = $('#creative_id').val();
	if (creative_id) {
		$.ajax({
			type: 'POST',
	      	url: '/admin/banners/links',
	      	dataType: 'json',
	      	data: {creative_id: creative_id},
		  	success: function(response) {
		  		if (response.status == true && response.links) {
		  			if (response.links.web) {
		  				$('#link-web').val(response.links.web).parents('.form-group').show();
		  			}
		  			if (response.links.android) {
		  				$('#link-android').val(response.links.android).parents('.form-group').show();
		  			}
		  			if (response.links.ios) {
		  				$('#link-ios').val(response.links.ios).parents('.form-group').show();
		  			}
		  		}
		  	},
		  	beforeSend: function() {
		  		$('#link-android').parents('.form-group').hide();
				$('#link-ios').parents('.form-group').hide();
				$('#link-web').parents('.form-group').hide();
		  	}
	  	});
	} else {
		$('#link-android').parents('.form-group').hide();
		$('#link-ios').parents('.form-group').hide();
		$('#link-web').parents('.form-group').hide();
	}
}

bannersTypeChange();
bannersCreativeIdChange();
bannersInitImage();

$(document).on('change', '#type', function(event) {
	bannersTypeChange();
});

$(document).on('change', '#creative_id', function(event) {
	bannersCreativeIdChange();
});

setInterval(function(){ bannersInitImage(); }, 100);