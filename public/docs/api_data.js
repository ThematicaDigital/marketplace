define({ "api": [
  {
    "type": "get",
    "url": "/banners",
    "title": "Get banners",
    "description": "<p>Request information about banners/videos</p>",
    "name": "GetBanners",
    "group": "Banners",
    "version": "1.0.2",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "..64",
            "optional": false,
            "field": "bundle_id",
            "description": "<p>Bundle ID of application</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "2..8",
            "allowedValues": [
              "\"ios\"",
              "\"android\""
            ],
            "optional": false,
            "field": "platform",
            "description": "<p>Device's platform</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "2",
            "optional": false,
            "field": "locale",
            "description": "<p>Device's locale</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "4..12",
            "optional": false,
            "field": "resolution",
            "description": "<p>Device's screen resolution</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "4..8",
            "allowedValues": [
              "\"wifi\"",
              "\"mobile\""
            ],
            "optional": false,
            "field": "connection_type",
            "description": "<p>Device's type connection</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"bundle_id\": \"i.mult.parovozov\",\n    \"platform\": \"android\",\n    \"locale\": \"en\",\n    \"resolution\": \"750x1560\",\n    \"connection_type\": \"wifi\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>Count of banners</p>"
          },
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "banners",
            "description": "<p>List of user banners information</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "banners.id",
            "description": "<p>Banner ID</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "banners.type",
            "description": "<p>Type of banner (&quot;video&quot; or &quot;banner&quot;)</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "banners.image",
            "description": "<p>Link of image for banner</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "banners.video",
            "description": "<p>Link of video for banner</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "banners.link",
            "description": "<p>Link of redirect for banner</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "banners.campaign_id",
            "description": "<p>Campaign ID</p>"
          },
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "campaigns",
            "description": "<p>List of campaigns</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "campaigns.id",
            "description": "<p>Campaign ID</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "size": "1-5",
            "optional": false,
            "field": "campaigns.priority",
            "description": "<p>Campaign priority</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\t\tHTTP/1.1 200 OK\n     {\n         \"status\": true,\n     \t\"count\": 2,\n     \t\"banners\": [\n             {\n                 \"id\": 1,\n                 \"type\": \"banner\",\n                 \"image\": \"files/uploads/900x1600.jpg\",\n                 \"video\": null,\n                 \"link\": \"https://play.google.com/store/apps/details?id=1\",\n\t\t\t\t\t\"campaign_id\": 3\n             },\n             {\n                 \"id\": 2,\n                 \"type\": \"video\",\n                 \"image\": \"files/uploads/750x1560.jpg\",\n                 \"video\": \"files/uploads/750x1560.avi\",\n                 \"link\": \"https://play.google.com/store/apps/details?id=1\",\n\t\t\t\t\t\"campaign_id\": 4\n             }\n         ],\n\t\t\t\"campaigns\": [\n\t\t\t\t{\n\t\t\t\t\t\"id\": 3,\n\t\t\t\t\t\"priority\": 1\n\t\t\t\t},\n\t\t\t\t{\n\t\t\t\t\t\"id\": 4,\n\t\t\t\t\t\"priority\": 5\n\t\t\t\t}\n\t\t\t]\n   \t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String[]",
            "optional": true,
            "field": "errors",
            "description": "<p>Error messages</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "\t\tHTTP/1.1 400 OK\n    \t{\n      \t\"status\": false,\n      \t\"errors\": {\n\t    \t\t\"bundle_id\": [\n\t\t      \t\t\"Поле bundle id обязательно для заполнения.\"\n\t\t    \t],\n\t    \t}\n\t    }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://45.77.43.157/api/v1/banners"
      }
    ],
    "filename": "app/Http/Controllers/Api/V1/BannersController.php",
    "groupTitle": "Banners"
  },
  {
    "type": "get",
    "url": "/frequence",
    "title": "Get frequence",
    "description": "<p>Request settings for the frequency of displaying banners/videos</p>",
    "name": "GetFrequences",
    "group": "Frequences",
    "version": "1.0.0",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "..64",
            "optional": false,
            "field": "bundle_id",
            "description": "<p>Bundle ID for application</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "2..8",
            "allowedValues": [
              "\"ios\"",
              "\"android\""
            ],
            "optional": false,
            "field": "platform",
            "description": "<p>Device's platform</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"bundle_id\": \"i.mult.parovozov\",\n    \"platform\": \"android\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "frequence_banner_time",
            "description": "<p>Interval in minutes between previews of banners in the application</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "frequence_banner_session",
            "description": "<p>Maximum number of banner previews per session</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "frequence_video_time",
            "description": "<p>Interval in minutes between previews of videos in the application</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "frequence_video_session",
            "description": "<p>Maximum number of video previews per session</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "frequence_other_time",
            "description": "<p>Interval in minutes between previews of other types in the application</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "frequence_other_session",
            "description": "<p>Maximum number of other type previews per session</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\tHTTP/1.1 200 OK\n     {\n     \t\"status\": true,\n     \t\"frequence_banner_time\": 1,\n     \t\"frequence_banner_session\": 2,\n     \t\"frequence_video_time\": 3,\n     \t\"frequence_video_session\": 4,\n     \t\"frequence_other_time\": 5,\n     \t\"frequence_other_session\": 6,\n   \t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String[]",
            "optional": true,
            "field": "errors",
            "description": "<p>Error messages</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "\tHTTP/1.1 400 OK\n    \t{\n     \t\"status\": false,\n      \t\"errors\": {\n\t    \t\t\"bundle_id\": [\n\t\t      \t\t\"Поле bundle id обязательно для заполнения.\"\n\t\t    \t],\n\t    \t}\n\t    }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://45.77.43.157/api/v1/frequence"
      }
    ],
    "filename": "app/Http/Controllers/Api/V1/FrequenceController.php",
    "groupTitle": "Frequences"
  },
  {
    "type": "put",
    "url": "/logs",
    "title": "Write logs",
    "description": "<p>Request to write to log of user's action.<br/><br/> Actions:</p> <ul> <li>view - when banner was started</li> <li>viewed - when banner was viewed before the end</li> <li>click - when user clicked on a banner</li> <li>redirect - when user was redirected to banner's link</li> </ul>",
    "name": "SetLog",
    "group": "Logs",
    "version": "1.0.0",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "..64",
            "optional": false,
            "field": "bundle_id",
            "description": "<p>Bundle ID for application</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "2..8",
            "allowedValues": [
              "\"ios\"",
              "\"android\""
            ],
            "optional": false,
            "field": "platform",
            "description": "<p>Device's platform</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..",
            "optional": false,
            "field": "banner_id",
            "description": "<p>Banner ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "4..8",
            "allowedValues": [
              "\"view\"",
              "\"viewed\"",
              "\"click\"",
              "\"redirect\""
            ],
            "optional": false,
            "field": "action",
            "description": "<p>User's action</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"bundle_id\": \"i.mult.parovozov\",\n    \"platform\": \"android\",\n    \"banner_id\": \"1\",\n    \"action\": \"click\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\tHTTP/1.1 200 OK\n     {\n     \t\"status\": true\n   \t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String[]",
            "optional": true,
            "field": "errors",
            "description": "<p>Error messages</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "\tHTTP/1.1 400 OK\n    \t{\n     \t\"status\": false,\n      \t\"errors\": {\n\t    \t\t\"bundle_id\": [\n\t\t      \t\t\"Поле bundle id обязательно для заполнения.\"\n\t\t    \t],\n\t    \t}\n\t    }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://45.77.43.157/api/v1/logs"
      }
    ],
    "filename": "app/Http/Controllers/Api/V1/LogsController.php",
    "groupTitle": "Logs"
  }
] });
